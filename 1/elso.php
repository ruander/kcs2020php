<!doctype html>
<html lang="hu">
<head>
 <title>Első php fileom</title>
 <meta charset="utf-8">
</head>
<body>
<?php 

//egy soros megjegyzés
/*
több 
soros 
megjegyzés
*/
print "Helo World! 2020";//kiírás stdo (standard output, jelen esetben a file ami éppen fut)
echo '<br>Új sor...';//Operátor: '' és "" string határoló

//változók kezelése - primitívek
$szoveg = 'Ez egy teszt szöveg...';//operátorok: = -> értékadó operátor; $ -> 'változó'; ; -> minden utasítást ; -el kell zárni || string - vagy szög tipusu változó
$szam = 123;// integer vagy int - egész szám
$lebegoPontos = 6/7;//floating point vagy float - lebego pontos szám
$logikai = true;//boolean vagy bool - logikai

//műveletek változókkal
//kiírás
echo '<br>';
echo $szoveg;
echo '<br>';
echo $szam;
echo '<br>';
echo $lebegoPontos;
echo '<br>';
echo $logikai;
//összefűzés
echo '<br>'.$szoveg;//operátor: . -> konkatenáció (szövegösszefűzés)
//összeadás
$eredmeny = $szam + $lebegoPontos;
echo '<br>'.$eredmeny;
echo '<br>'.($szam - $lebegoPontos);
echo '<br>'.($szam / $lebegoPontos);
echo '<br>'.($szam * $lebegoPontos);
/////////////beépített eljárások
$veletlenSzam = rand();//0-getrandmax() közé generál véletlen számot
echo '<h2>'.$veletlenSzam.'</h2>';
$veletlenSzam = rand(1,6);//változó felülírása - variable override
echo "<h2>$veletlenSzam</h2>";//"" -en belül a változókat értelmezi a php modul
echo "<h2>\$veletlenSzam</h2>";// operátor: \ -> escape, az utána közvetlenül következő karaktert kilépteti a végrehajtható elemek közül

//bemutató példa: kockadobás
$dobas = rand(1,6);
echo "<h2>A dobás értéke: $dobas</h2>";

//ha páros kékkel írjuk ki, egyébként pirossal
$dobas = rand(1,6);
/*
if(feltétel){
	igaz ág
}else{
	hamis ág
}
*/
if($dobas%2 == 0){
	//páros 
	echo "<h2 style='color:blue;'>A dobás értéke: $dobas</h2>";
}else{
	//páratlan
	echo "<h2 style='color:red;'>A dobás értéke: $dobas</h2>";
}



 ?>
</body>
</html>