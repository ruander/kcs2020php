<?php

if(!empty($_POST)){
    $szoveg = filter_input(INPUT_POST,'text');
    echo ekezettelenit($szoveg);
    echo '<br>'.ekezettelenitFileName($szoveg);
    //inputból érkez - -ek eltávolítása
    $szoveg = str_replace("-","",$szoveg);
    echo '<br>Most a szöveg: '.$szoveg;
    echo '<br>'.strtoupper(hyphenate(ekezettelenit($szoveg)));//megjelenítéshez

}

//megjelenítéshez '-' hozzáadása a tokenhez
function hyphenate($str)
{
    return implode("-", str_split($str, 6));
}

/*
 * string kezelő eljárások
 */
/**
 * @param $str
 * @return string
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

/**
 * @param $str
 * @return string
 */
function ekezettelenitFileName($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kuponmező - javascripttel</title>
    <style>
        label {
            display: flex;
            flex-direction: column;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        Szöveg: <input maxlength="27" onkeyup="addHyphen(this)" type="text" name="text" placeholder="valami..." value="<?php echo filter_input(INPUT_POST,'text'); ?>" id="text">
    </label>
    <button>Mehet</button>
</form>
<script>

    /**
     * Beviteli mező helper
     * @param element
     */
    function addHyphen (element) {
        let ele = document.getElementById(element.id);
        let myEl = ele;//későbbre
        if (ele.value.length > 6) {//ha elég hosszu a string csak akkor babráljuk
            ele = ele.value.split('-').join('');    // Remove dash (-) if mistakenly entered.
            let finalVal = ele.match(/.{1,6}/g).join('-');
            myEl.value =minusEkezet(finalVal).replace(/[^a-zA-Z0-9-]/g, "").toUpperCase();
        }else{
            myEl.value =minusEkezet(ele.value).replace(/[^a-zA-Z0-9-]/g, "").toUpperCase();
        }
    }

    /**
     * Lecseréli az ékezetes betűket nem ékezetesre
     * @param text
     * @returns {*}
     * @constructor
     */
    function minusEkezet(text) {
        text = text.replace(/á/g, 'a');
        text = text.replace(/é/g, 'e');
        text = text.replace(/í/g, 'i');
        text = text.replace(/ó/g, 'o');
        text = text.replace(/ö/g, 'o');
        text = text.replace(/ő/g, 'o');
        text = text.replace(/ú/g, 'u');
        text = text.replace(/ü/g, 'u');
        text = text.replace(/ű/g, 'u');
        text = text.replace(/Á/g, 'A');
        text = text.replace(/É/g, 'E');
        text = text.replace(/Í/g, 'I');
        text = text.replace(/Ó/g, 'O');
        text = text.replace(/Ö/g, 'O');
        text = text.replace(/Ő/g, 'O');
        text = text.replace(/Ú/g, 'U');
        text = text.replace(/Ü/g, 'U');
        text = text.replace(/Ű/g, 'U');
        return text;
    }
</script>
</body>
</html>
