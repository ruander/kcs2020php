<?php
    var_dump('<pre>',$_POST,'</pre>');
if (!empty($_POST)) {
    $hiba = [];//itt lesznek a hibák
    //N szám vizsgálata
    $number = filter_input(INPUT_POST, '_N', FILTER_VALIDATE_INT);
    //most nem szűrővel, hanem egy elágazással nézzük meg pozitív-e
    if ($number < 1) {
        $hiba['_N'] = '<span class="error">Hibás formátum!</span>';
    }

    //ha kapunk szam elemet a postba akkor az a feladat 11 ből jön
    $szamok = filter_input(INPUT_POST,'szamok',FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    if($szamok){
        //van mit hibakezelni
        foreach($szamok as $k => $v){
            if( filter_var($v,FILTER_VALIDATE_INT) === false ){
                $hiba['szamok'][$k] = '<span class="error">Hibás formátum!</span>';
            }
        }
    }
    var_dump($hiba);
    //ha nincs hiba
    if (empty($hiba)) {
        //feladat 8
        $output_8 = '';
        $string_8 = 'XO';
        //megoldás ciklussal
        for ($i = 0; $i < $number; $i++) {
            $output_8 .= $string_8;
        }
        $output_8 .= '<br>';
        //feladat 9
        $output_9 = '';
        $string_9 = 'OX';
        //megoldás ciklussal
        for ($i = 0; $i < $number; $i++) {
            $output_9 .= $string_9;
        }
        $output_9 .= '<br>';
        //az output_8 még kell elé
        $output_9 = $output_8 . $output_9;

        //feladat 10
        $output_10 = '';
        for($i=0;$i<$number;$i++){
            $output_10 .= $output_9;
        }
        $output_10 .= "<br>";
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Gyakorlás - feladatmegoldások</title>
    <style>
        .error {
            color: red;
            font-style: italic;
            font-size: 0.7em;
        }
        label {
            display:block;
        }
    </style>
</head>
<body>
<main>
    <h1>Gyakorlás - feladatmegoldások</h1>
    <form method="post">
        <label>
            Adj meg egy N<sup>+</sup> természetes számot
            <input type="text" name="_N" value="<?php echo filter_input(INPUT_POST, '_N') ?>" placeholder="5" size="2"
                   maxlength="2">
            <?php
            //ha hiba van kiírjuk
            if (isset($hiba['_N'])) {
                echo $hiba['_N'];
            }
            ?>
        </label>
        <button>Gyí</button>
    </form>
    <article>
        <h2>8. Készítsünk programot, amely bekér egy N természetes számot, majd kirajzol a képernyőre egymás mellé
            N-szer az "XO" betűket és a kiírás után a kurzort a következő sor elejére teszi.</h2>
        <h3>Megoldás ha kaptunk jó adatot</h3>
        <p>
            <?php
            //ha létezik megoldás kiírjuk
            if (isset($output_8)) {
                echo $output_8;
            } else {
                echo "Nincs még érékelhető megoldás vagy rossz az adat!";
            }
            ?>
        </p>
    </article>
    <article>
        <h2>9. Egészítsük ki az előző programunkat úgy, hogy az előző kiírás alá írja ki N-szer az "OX" betűket is egymás mellé, majd a kurzort ismét a következő sor elejére tegye. (Az előző ciklus után - NE bele a ciklusba! - tegyünk egy hasonló ciklust, ami most XO helyett OX betűket ír ki.)</h2>
        <h3>Megoldás ha kaptunk jó adatot</h3>
        <p>
            <?php
            //ha létezik megoldás kiírjuk
            if (isset($output_9)) {
                echo $output_9;
            } else {
                echo "Nincs még érékelhető megoldás vagy rossz az adat!";
            }
            ?>
        </p>
    </article>
    <article>
        <h2>10. Egészítsük ki a programunkat úgy, hogy az előző két sort N-szer ismételje meg a program. (Az előző két egymás utáni ciklust tegyük bele egy külső ciklusba.) </h2>
        <h3>Megoldás ha kaptunk jó adatot</h3>
        <p>
            <?php
            //shorten, short hand, rövid elágazás
            echo isset($output_10)? $output_10:'Nincs még érékelhető megoldás vagy rossz az adat!';
            ?>
        </p>
    </article>
    <article>
        <h2>11. Készítsünk programot, amely beolvas egy N természetes számot, majd billentyűzetről bekér N darab. természetes számot és ezeket a számokat összeadja, majd kiírja az eredményt. (Vegyünk egy változót, amit a program elején kinullázunk. Ehhez a cikluson belül mindig adjuk hozzá az éppen beolvasott számot. A szám beolvasása a ciklusban lehet N-szer ugyanabba a változóba, hiszen miután hozzáadtuk az összeghez, már nincs rá szükségünk, tehát használhatjuk a következő szám beolvasására.)</h2>
        <h3>Ha kaptunk már egy természetes számot, itt lesznek az input mezők</h3>
        <p>
            <?php
            //ha van természetes jó számunk (2+)
            if(isset($number) and $number > 1){
                //segédváltozóban állítjuk össze az űrlapot
                $form_11 = '<form method="post">';//form nyitás
                //kérek N-nyi ($number) input mezőt belefűzni az űrlapunkba
                for($i=1;$i<=$number;$i++) {
                    $form_11 .= '<label>
            Add meg az ' . $i . '. számot
            <input type="text" name="szamok[' . $i . ']" value="" placeholder="5" size="2"
                   maxlength="2">'.(isset($hiba['szamok'][$i]) ? $hiba['szamok'][$i] : '')
        .'</label>';
                //@todo a mezők tartsák meg a beírt adatokat
                //@todo ha minden szám jó, írjuk ki az összeadás eredményét

                }//end for


                //ha van _N a másik ürlapról, vegyük át az értéket egy hidden mezőbe
                $form_11 .= '<input type="hidden" name="_N" value="'.filter_input(INPUT_POST,'_N').'">';
                $form_11 .=  '<button>Ezt add öszze</button></form>';//form zárás és a gomb
                echo $form_11;//egy lépésben kiírjuk, ahol kell
            }else{
                echo 'Nincs még vagy nem jó az N szám.';
            }
            ?>
        </p>
    </article>
</main>
</body>
</html><?php