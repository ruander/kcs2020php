<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Táblázat készítése ciklus segítségével - beágyazott ciklus</title>
</head>
<body>
<?php
$sor = 10;//ennyi sort szeretnék
$oszlop = 3;//ennyi cella egy sorban
//a cellákban legyen a nagy X betű
//egy változóba gyűjtjük amit ki szeretnénk írni
$table = '<table border="1">';//tábla nyitása
for($i=1;$i<=$sor;$i++){
//ciklus a soroknak
    $table .='<tr>';//sor TAG  nyitás

        for($j=1;$j<=$oszlop;$j++){//belső vagy beágyazott ciklus a celláknak
            $table .= "<td>$i X $j</td>"; //cella
        }

    $table .='</tr>';//sor TAG  zárás
}

$table .= '</table>';//tábla zárása
//kiírás egy lépésben:
echo $table;
/**
 * @todo: készíts egy érvényes lotto sorsoló programot 5/90.
 * a régi folyamat automatizálása
 */
?>
</body>
</html>
