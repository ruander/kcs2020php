<?php
$tomb = ['béla','kati','feri','éva'];
//for ciklus javascriptes mintára a tömb elemeinek kiírására
for($i=0; $i<count($tomb); $i++){
    echo "<br>$i. elem: $tomb[$i]";
}
//tömb bővítése irányított asszociativ indexre
$tomb['id'] = 'Gyuri';
echo '<pre>'.var_export($tomb, true).'</pre>';
//tömb bejárása
foreach($tomb as $key => $value){
    echo "<br>$key elem: $value";
}

//echo $tomb['id'];//notice, mert nincs olyen kulcs a tömbnek
//kiírás, felhasználás előtt teszteljük létezik e a változó amit használunk
if(array_key_exists('id',$tomb)){
    //létezik a kulcs
    echo $tomb['id'];
}
//vizsgálat másképpen
if( isset($tomb['id']) ){
    echo $tomb['id'];
}

$tomb = [12,134,51,567,546,43];
//tömb értékek összege
echo '<pre>'.var_export($tomb, true).'</pre>';
echo "Az értékek összege: ".array_sum($tomb);
//tömb értékek összege más módszerrel (array_sum nélkül)
$osszeg = 0;
foreach ($tomb as $v){
    $osszeg +=$v;
}
echo "<br>A tömb értékeinek összege:".$osszeg;

//készítsünk egy tömböt véletlenszerű 40-100 közötti értékekkel, aminek 30 eleme van
$tomb=[];//itt lesznek az értékek
for($i=1;$i<=30;$i++){
    $tomb[$i] = rand(40,100);//tömb feltöltése 1 elemmel
}
echo '<pre>'.var_export($tomb, true).'</pre>';
echo "Az értékek összege: ".array_sum($tomb);
//sorrendezés értékek alapján - novekvő
$tomb['teszt']=500;
sort($tomb);//referencia átadás
echo '<pre>'.var_export($tomb, true).'</pre>';
//ismétlődése törlése /egyedi értékek/
$tomb = array_unique($tomb);
echo '<pre>'.var_export($tomb, true).'</pre>';

////készítsünk egy tömböt véletlenszerű 40-100 közötti értékekkel, aminek 30 egyedi értékű eleme van
$tomb = [];
while(count($tomb)<30){
    $tomb[] = rand(40,100);//tömb feltöltése 1 elemmel
    $tomb = array_unique($tomb);//ismétlődés(ek) eltávolítása
}
echo '<pre><b></b>'.var_export($tomb, true).'</pre>';
/*
while(feltétel){
    ciklusmag
}
//hátultesztelő változata
do{
    ciklusmag
}while(feltétel)
 */
