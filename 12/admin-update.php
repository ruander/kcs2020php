<?php
    //@todo : folyamatábra mindhárom fileról +megérteni a folyamatokat
require "connect.php";   //db kapcsolat
//felhasználó vagy admin módosítása
$tid = filter_input(INPUT_GET,'id',FILTER_VALIDATE_INT);
//ha van adat, lekérjük
if($tid){
    $qry = "SELECT * FROM admins WHERE id = $tid LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);

}else{
    //nincs id, nincs mit editálni
    die('Ne babráld az urlt!');
}
if (!empty($_POST)) {
    $hiba = [];
    //username min 3 kar
    $username = trim(filter_input(INPUT_POST, 'username'));
    //szövegvégi spacek eltávolításával
    if (mb_strlen($username, 'utf-8') < 3) {
        $hiba['username'] = '<span class="error">Kötelező kitölteni (min 3 karakter)!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    } else {
        //foglalt-e már a db-ben
        $qry = "SELECT id FROM admins WHERE email  = '$email' LIMIT 1 ";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row_email = mysqli_fetch_row($result);//a [0] van az id, ha van, de most már az is baj ha egyáltalán van row
        //var_dump($row_email,$tid);
        if ($row_email AND $row_email[0] != $tid) {
            $hiba['email'] = '<span class="error">Foglalt email cím!</span>';
        }
    }
    //jelszó min 6 karakter kell legyen
    /*
     * update átalakítás működése
     * ha az 1 es jelszó mezőben akár 1 karakter szerepel, hibakezeljük, egyébként nem módosítjuk a jelszót
     */
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if(strlen($pass)>0) {
        if (mb_strlen($pass, "utf-8") < 6) {
            $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
        } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
            $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
        }
    }
    //status
    $status = filter_input(INPUT_POST, 'status',FILTER_VALIDATE_INT)?:0;//a mező hack ellen a FILTER_VALIDATE_INT véd jelen esetben, így nem kell real_escape

    if (empty($hiba)) {
        //adatok 'tisztázása'
        $data = [
            'username' => mysqli_real_escape_string($link,$username),
            'email' => $email,
            'status' => $status,
            'time_updated' => date('Y-m-d H:i:s')
        ];
        //ha üres a $pass akkor nem, de ha nem üres, akkor benne kell legyen a tömbben
        if($pass) $data['pass']=$pass;
        //kérés összeállítása
        /*
        SET összeállítása data bejárással
        $update_set = ...
        */
        $upd = [];
        foreach($data as $field => $v){
            $upd[] = "`$field` = '$v'";
        }
        $update_set = implode(',',$upd);
        $qry = "UPDATE `admins` 
                    SET 
                    $update_set
                WHERE `id` = $tid
                LIMIT 1";

        mysqli_query($link, $qry) or die(mysqli_error($link));
        //átirányítás a listára
        header("location:admin-list.php");
        //echo '<pre>' . var_export($data, true) . '</pre>';
        exit();
    }
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Admin szerkesztése</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form.registration {
            max-width: 640px;
            padding: 15px;
            display: flex;
            flex-flow: column nowrap;
        }

        .registration > label:not(.status) {
            display: flex;
            flex-flow: column nowrap;
            margin: 10px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>
</head>
<body>
<div class="container">
    <form method="post" class="registration">
        <h1>Admin szerkesztése --<?php echo $tid ?>--</h1>
        <a href="admin-list.php">Mégse</a>
        <label>
            <span>Név<sup>*</sup></span>
            <input
                    type="text"
                    name="username"
                    placeholder="John Doe"
                    value="<?php echo getValue('username', $row); ?>">
            <?php
            echo hibaKiir('username');
            ?>
        </label>
        <label>
            <span>Email<sup>*</sup></span>
            <input
                    type="text"
                    name="email"
                    placeholder="your@email.com"
                    value="<?php echo getValue('email', $row); ?>">
            <?php
            echo hibaKiir('email');
            ?>
        </label>
        <label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="pass" placeholder="******" value="">
            <?php
            echo hibaKiir('pass');
            ?>
        </label>
        <label>
            <span>Jelszó újra<sup>*</sup></span>
            <input type="password" name="repass" placeholder="******" value="">
            <?php
            echo hibaKiir('repass');
            ?>
        </label>
        <label class="status">
            <input type="checkbox" name="status"
                   value="1" <?php
            /*kipipálva, ha
            1. poston jön status
            2. row[status]=1 és nincs post TÖMB!
            -nincs kipipálva, ha
            1. row[status]=0 és nincs post vagy nincs benne status
            2. row[status]=1 és van post ,de nincs benne status
            */

                   echo (filter_input(INPUT_POST,'status')
                    OR
                    $row['status'] == 1 AND empty($_POST))?'checked':'';

            ?>> aktív?
        </label>
        <button>Módosítom</button>
    </form>
</div>
</body>
</html><?php
/*saját eljárások gyüjteménye*/
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

/**
 * Mezőértékek visszaadása mezőnév alapján + ha van adatbázis row akkor az az adat tér vissza vagy semmi
 * @param $fieldName :string | a mező neve
 * @param  $row :array | a lekért adatok asszociatív tömbje az adatbázisból
 * @return string - beírandó érték a mezőbe
 */
function getValue($fieldName, $row = [])
{
    $ret = filter_input(INPUT_POST, $fieldName);
    if($ret !== NULL ){//ha találtunk ilyen elemet (akár üresen is)
        return $ret;
    }elseif( isset($row[$fieldName]) ){//ha van ilyen db adat
        return $row[$fieldName];
    }
    return false;
}
