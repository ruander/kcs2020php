<?php
require "connect.php";
//erőforrások
//url adatok
$tid = filter_input(INPUT_GET,'id',FILTER_VALIDATE_INT);//delete ?id=n
//////////
//ha kell, törlünk
if($tid){
    mysqli_query($link, "DELETE FROM admins WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
    //hogy ne maradjon url paraméter
    header('location:'.$_SERVER['PHP_SELF']);
    exit();
}
//admin sorok lekérése
$qry = "SELECT id, username,email,lastlogin,status FROM admins";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
//táblázat elkészítése
$table = '<a href="admin-create.php">Új felvitel</a><table border="1">';

while ($row = mysqli_fetch_assoc($result)){
    $table .= '<tr>';
    $table .= '<td>'.$row['id'].'</td>';
    $table .= '<td>'.$row['username'].'</td>';
    $table .= '<td>'.$row['email'].'</td>';
    $table .= '<td>'.$row['lastlogin'].'</td>';
    $table .= '<td>'.$row['status'].'</td>';
    //crud
    $table .='<td><a href="admin-update.php?id='.$row['id'].'">edit</a> | <a href="?id='.$row['id'].'" onclick="return confirm(\'Are you sure to delete?\')">delete</a></td>';
    $table .= '</tr>';
}
$table .= '</table>';
echo $table;