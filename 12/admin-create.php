<?php
require "connect.php";   //db kacpsolat
//felhasználó vagy admin regisztrációja
if (!empty($_POST)) {
    $hiba = [];
    //username min 3 kar
    $username = trim(filter_input(INPUT_POST, 'username'));
    //szövegvégi spacek eltávolításával
    if (mb_strlen($username, 'utf-8') < 3) {
        $hiba['username'] = '<span class="error">Kötelező kitölteni (min 3 karakter)!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    } else {
        //foglalt-e már a db-ben
        $qry = "SELECT id FROM admins WHERE email  = '$email' LIMIT 1 ";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);//a [0] van az id, ha van, de most már az is baj ha egyáltalán van row
        if ($row) {
            $hiba['email'] = '<span class="error">Foglalt email cím!</span>';
        }
    }
    //jelszó min 6 karakter kell legyen
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if (mb_strlen($pass, "utf-8") < 6) {
        $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
    } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
        $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
    } else {
        $pass = password_hash($pass, PASSWORD_BCRYPT);
        //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
    }
    //termst kötelező elfogadni
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező elfogadni!</span>';
    }

    if (empty($hiba)) {
        //adatok 'tisztázása' és mysql injection elleni védelem, ahova kell
        $data = [
            'username' => mysqli_real_escape_string($link, $username),
            'email' => $email,
            'pass' => $pass,
            'status' => 1,
            'time_created' => date('Y-m-d H:i:s')
        ];
        //kérés összeállítása
        /*echo $qry = "INSERT INTO
                    `admins`(
                        `username`,
                        `email`,
                        `pass`,
                        `status`,
                        `time_created`
                    )
                    VALUES (
                        '{$data['username']}',
                        '{$data['email']}',
                        '{$data['pass']}',
                        '{$data['status']}',
                        '{$data['time_created']}'
                    )";*/
        $fieldNames = '`' . implode('`,`', array_keys($data)) . '`';
        $values = "'" . implode("','", $data) . "'";
        $qry = "INSERT INTO 
                    `admins`( $fieldNames ) 
                    VALUES ( $values )";
        //echo '<pre>' . var_export($data, true) . '</pre>';
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //átirányítás a listára
        header("location:admin-list.php");
        exit();
    }
}


?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Regisztráció</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form.registration {
            max-width: 640px;
            padding: 15px;
            display: flex;
            flex-flow: column nowrap;
        }

        .registration > label:not(.terms) {
            display: flex;
            flex-flow: column nowrap;
            margin: 10px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 11px;
        }
    </style>
</head>
<body>
<div class="container">
    <?php
    $form = '<form method="post" class="registration">
        <h1>Regisztráció</h1>';
    //username
    $form .= '<label>
            <span>Név<sup>*</sup></span>
            <input
                    type="text"
                    name="username"
                    placeholder="John Doe"
                    value="' . getValue('username') . '">'
        . hibaKiir('username') . '</label>';
    //email
    $form .= '<label>
            <span>Email<sup>*</sup></span>
            <input
                    type="text"
                    name="email"
                    placeholder="your@email.com"
                    value="' . getValue('email') . '">' .
        hibaKiir('email') . '</label>';
    //pass
    $form .= '<label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="pass" placeholder="******" value="">' . hibaKiir('pass') . '</label>';
    //repass
    $form .= '<label>
            <span>Jelszó újra<sup>*</sup></span>
            <input type="password" name="repass" placeholder="******" value="">' . hibaKiir('repass') . '</label>';
    //terms
    $form .= '<label class="terms">
            <input type="checkbox" name="terms"
                   value="1" ' . (filter_input(INPUT_POST, 'terms') ? 'checked' : '') . '> Elolvastam, megértettem
            és elfogadom az <a href="#" target="_blank">adatkezelési irányelveket</a>.<sup>*</sup>' . hibaKiir('terms') . '</label>';
    //submit
    $form .= '<button>Regisztrálok</button>
    </form>';

    echo $form;
    ?>
</div>
</body>
</html><?php
/*saját eljárások gyüjteménye*/
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

/**
 * @param $fieldName :string | a mező neve
 * @param false $subKey :mixed | ha tömbben vannak az értékek a postban akkor 2 szint kulcsa
 * @return string - beírandó érték a mezőbe
 */
function getValue($fieldName, $subKey = false)
{
    //ha kapunk subkey-t, akkor szűrő kell
    if ($subKey !== false) {
        $elem = filter_input(INPUT_POST, $fieldName, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);//leszűrjük a tömbböt default szűrővel
        if (isset($elem[$subKey])) return $elem[$subKey];//visszatérünk a kívánt elemmel
    } else {//nem kaptunk subkeyt tehát első szinten megtaláljuk az adatot postban, visszatérünk vele
        return filter_input(INPUT_POST, $fieldName);
    }
    return false;
}
