<?php
//állandók megadása
const EZ_EGY_ALLANDO = 555;
echo EZ_EGY_ALLANDO;
//az alábbi minta is működik, de rossz szemmel nézik
const ez_is_egy_allando = 29634;
echo '<br>'.ez_is_egy_allando;
// $valtozoNeveAmiHosszuIsLehet - camel case
// $valtozo_neve_ami_hosszu_is_lehet - snake case
// $valtozo_2_neve_ami_hosszu_is_lehet - snake case
//EZ_EGY_ALLANDO = 777; - állandót nem lehet felülírni, módosítani !!!
const VALID_GAME_TYPES = [
    5 => 90,
    6 => 45,
    7 => 35
];
var_dump(VALID_GAME_TYPES);