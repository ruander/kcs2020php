<?php

var_dump(szamGeneralas());
/**
 * Lottószám generálása bármilyen lottóra
 * @param int $huzasok_szama
 * @param int $limit
 * @return array
 */
function szamGeneralas($huzasok_szama = 5 , $limit = 90){
    $szamok = [];//itt lesznek a számok
    //$huzasok_szama = 5;
    //$limit = 90;
    //ha a huzások száma nagyobb mint a limit akkor végtelen ciklusba kerülünk, ezért ezt az esetet vizsgálni kell
    if($limit < $huzasok_szama){
        //gond van, a while végtelen ciklusba futna
        //die('rossz értékek az eljárásban');//azonnal megáll a kód futás, és kiirja a stdo (standard output) -ra a stringet amit adunk neki
        trigger_error('A szamGeneralas eljárás hibás paraméterekkel lett meghívva',E_USER_ERROR);
        //return false;
    }
    while (count($szamok) < $huzasok_szama) {
        $szamok[] = rand(1, $limit);
        //az ismétlődések eltávolítása
        $szamok = array_unique($szamok);
    }
    //megvannak a számok, rendezzük sorba
    sort($szamok);
    //térjünk vissza a kapott tömbbel
    return $szamok;
}

echo 'teszt';