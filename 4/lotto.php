<?php
//mintaEljaras();//eljárás hívása történhet a deklarálása előtt
//lottójáték folyamatának megvalósítása
//5 különböző szám 1-90 között
//erőforrások
$szamok = [];//itt lesznek a számok
$huzasok_szama = 6;
$limit = 45;
while (count($szamok) < $huzasok_szama) {
    $szamok[] = rand(1, $limit);
    //az ismétlődések eltávolítása
    $szamok = array_unique($szamok);
}

sort($szamok);//emelkedő értéksorrend

echo '<pre>' . var_export($szamok, true) . '</pre>';

//saját eljárás készítése
/*
function eljarasNeve(param1=value1,param2, ...){
    //programkód
}
*/
//minta eljárás 'deklarálás'
function mintaEljaras(){
    echo 'Fut a mintaeljárás!';
}
//mintaEljaras();//eljárás hívása
//minta 2 (void)
function mintaEljaras2($text = 'default text'){
    echo "Fut a 2. mintaEljaras ezt a paramétert kaptuk: $text";
}
mintaEljaras2();
//minta 3
//ez az eljárás nem void, van visszatérése, ami egy string tipusú érték lesz
/**
 * Kiemelt szöveg készítésére eljárás
 * @param string $text
 * @return string
 */
function mintaEljaras3($text = 'Ez a minta 3.'){
    $ret = "<b>$text</b>";
    return $ret;
}
echo mintaEljaras3('paraméter szöveg');//nincs hatása

function szamGeneralas(){
    $szamok = [];//itt lesznek a számok
    $huzasok_szama = 5;
    $limit = 90;
    while (count($szamok) < $huzasok_szama) {
        $szamok[] = rand(1, $limit);
        //az ismétlődések eltávolítása
        $szamok = array_unique($szamok);
    }
    //megvannak a számok, rendezzük sorba
    sort($szamok);
    //térjünk vissza a kapott tömbbel
    return $szamok;
}
echo '<pre>';
$tippsor = szamGeneralas();
var_dump($tippsor);
///////////////
//a sorsolás
$sorsolas = szamGeneralas();
var_dump($sorsolas);
//találatok megállapítása (metszet képzése)
$talalatok = array_intersect($tippsor,$sorsolas);
var_dump($talalatok);
//eredmények rendezése, kiírások
$talalatok_szama = count($talalatok);

echo "<h2>A tippsor számai: ".implode(',',$tippsor)."</h2>";
echo "<h2>A sorsolás számai: ".implode(',',$sorsolas)."</h2>";
//a találatok száma alapján végezzük el a kiírásokat
if($talalatok_szama > 0){
    //van találat
    echo "<h2>Találatok száma: $talalatok_szama</h2>";
    echo "<h2>Eltalált számok: ".implode(',',$talalatok)."</h2>";
}else{
    //nincs találat
    echo "<h2>Sajtos ropogós! :( </h2>";
}



