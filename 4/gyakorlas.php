<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Házi feladatok txt file megoldásai</title>
</head>
<body>
<p>14.Írjon egy programot, amely kiszámolja az 1-től 10-ig terjedő egész számok átlagát ciklus utasítás
    használatával.</p>
<?php
$limit = 10;
$sum = 0;
for ($i = 1; $i <= $limit; $i++) {
    $sum += $i;
}
/*
echo $i--;//$i itt 11 mert ezért nem léptünk be a ciklusba
echo "<br>$i";
*/
$atlag = $sum / --$i;// --$i a művelet előtt csökken 1el $i értéke
echo "A számok átlaga 1-től $limit-ig => $atlag";
?>
<p>19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
    <br>2
    <br>22
    <br>222
    <br>2222</p>
<?php
//beágyazott ciklus
$char = '2';
for($i=1;$i<=4;$i++){
    echo '<br>';
    //belső ciklus a karakter kiírására '2':
    for($j=1;$j<=$i;$j++){
        echo $char;
    }
}
//másképpen picit
for($i=1;$i<=4;$i++){
    //str_repeat
    echo '<br>'.str_repeat($char,$i);
}
//20as feladat
//beágyazott ciklussal
for($i=4;$i>=1;$i--){
    echo '<br>';
    //belső ciklus a karakter kiírására '2':
    for($j=1;$j<=$i;$j++){
        echo $char;
    }
}
//str_repeat
for($i=4;$i>=1;$i--){
    echo '<br>'.str_repeat($char,$i);
}
//@todo: hazi-feladatok.txt 14-24
?>
</body>
</html>
