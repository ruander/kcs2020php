<?php
//file betöltése, mintha ide lenne gépelve
/*include "settings.php";*/
include_once "settings.php";
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Lottójáték - válassz játéktípust</title>
</head>
<body>
<h1>Lottójáték</h1>
<h2>Válassz játéktípust</h2>
<?php
/*ciklussal oldjuk meg a játéktipusok menüvé alakítását*/
$menu =  '<nav><ul>';//menu elemek nyitása
foreach($valid_gametypes as $k => $v){
    $menu .= '<li><a href="lotto.php?gametype='.$k.'">'.$k.'/'.$v.' játék</a></li>';
}
//menuelemek zárása
$menu .= '</ul></nav>';
// kiírás 1 lépésben
echo $menu;
?>
</body>
</html>