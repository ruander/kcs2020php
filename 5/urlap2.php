<?php
//ha van adat a POSTBAN akkor dolgozzuk fel
if( !empty($_POST) ){
    $hiba = [];//ide gyüjtjük a hibákat
    //adatok kinyerése és hibakezelés
    //név legyen minimum 3 karakter
    $nev = filter_input(INPUT_POST, 'nev');
    //kezdő és záró felesleges spacek eltávolítása
    $nev = trim($nev);//ltrim,rtrim
    if( mb_strlen($nev,'utf-8') < 3){
        $hiba['nev'] = '<span class="error">Név minimum 3 karakter kell legyen!</span>';
    }

    //email kötelező
    $email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);
    if( !$email){
        $hiba['email'] =  '<span class="error">Hibás formátum!</span>';
    }

    //terms kötelező kipipálni
    $terms = filter_input(INPUT_POST, 'terms');
    if( !$terms){
        $hiba['terms'] = '<span class="error">Kötelező elfogadni!</span>';
    }

    //uzenetmező
    $msg = filter_input(INPUT_POST, 'message');
    echo '<pre>';
    var_dump($_POST,$hiba);
    echo '</pre>';

    if(empty($hiba)){//ha eljutunk ide és még üres a hibatömb, akkor nincs hibánk
        //hf: @todo - feladatgyujtemeny pdf : 8-11 + 20as
        die('nincs hiba');
    }

}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozása azonos fileban</title>
    <style>
        form {
            width:450px;
            margin:15px auto;
            display:flex;
            flex-flow: column nowrap;
        }
        h1 {
            text-align: center;
        }
        .error {
            color:red;
            font-style:italic;
            font-size:0.7em;
        }
    </style>
</head>
<body>
<h1>Űrlap feldolgozása azonos fileban</h1>
<form method="post">
    <label> Név
        <input type="text" name="nev" placeholder="Gipsz Jakab" value="<?php echo filter_input(INPUT_POST,'nev'); ?>">
        <?php
        //ha létezik a hiba tömb adott kulcsa akkor kiírjuk
        if(isset($hiba['nev'])) {
            echo $hiba['nev'];
        }
        ?>
    </label>
    <label> Email
        <input type="text" name="email" placeholder="emai@cim.hu" value="<?php echo filter_input(INPUT_POST,'email'); ?>">
        <?php
        //ha létezik a hiba tömb adott kulcsa akkor kiírjuk
        if(isset($hiba['email'])) {
            echo $hiba['email'];
        }
        ?>
    </label>
    <label>
        <input type="checkbox" name="terms" value="1"> Elolvastam és megértettem az adatvédelmi tájékoztatót!
        <?php
        //ha létezik a hiba tömb adott kulcsa akkor kiírjuk
        if(isset($hiba['terms'])) {
            echo $hiba['terms'];
        }
        ?>
    </label>
    <label>
        Egyéb üzenet<br>
        <textarea rows="5" cols="50" name="message" placeholder="egyéb üzenet..."><?php echo filter_input(INPUT_POST,'message'); ?></textarea>
    </label>
    <button>Elküld</button>
</form>
</body>
</html>
