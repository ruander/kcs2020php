<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozása</title>
    <style>
        form {
            display:flex;
            flex-flow: column nowrap;
        }
        label {

        }
    </style>
</head>
<body>
<h1>Űrlap 1 - get metódus</h1>
<form method="get" action="feldolgoz.php">
    <label>
        <input type="text" name="nev" placeholder="Gipsz Jakab">
    </label>
    <label>
        <input type="checkbox" name="terms" value="1"> Elolvastam és megértettem az adatvédelmi tájékoztatót!
    </label>
    <label>
        Egyéb üzenet<br>
        <textarea rows="5" cols="50" name="message" placeholder="egyéb üzenet..."></textarea>
    </label>
    <button>Elküld</button>
</form>
<h1>Űrlap 2 - post metódus</h1>
<form method="post" action="feldolgoz.php">
    <label> Név
        <input type="text" name="nev" placeholder="Gipsz Jakab">
    </label>
    <label> Email
        <input type="text" name="email" placeholder="emai@cim.hu">
    </label>
    <label>
        <input type="checkbox" name="terms" value="1"> Elolvastam és megértettem az adatvédelmi tájékoztatót!
    </label>
    <label>
        Egyéb üzenet<br>
        <textarea rows="5" cols="50" name="message" placeholder="egyéb üzenet..."></textarea>
    </label>
    <button>Elküld</button>
</form>

</body>
</html>
