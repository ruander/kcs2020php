<?php
/*saját eljárások gyüjteménye*/
/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

/**
 * Mezőértékek visszaadása mezőnév alapján + ha van adatbázis row akkor az az adat tér vissza vagy semmi
 * @param $fieldName :string | a mező neve
 * @param  $row :array | a lekért adatok asszociatív tömbje az adatbázisból
 * @return string - beírandó érték a mezőbe
 */
function getValue($fieldName, $row = [])
{
    $ret = filter_input(INPUT_POST, $fieldName);
    if($ret !== NULL ){//ha találtunk ilyen elemet (akár üresen is)
        return $ret;
    }elseif( isset($row[$fieldName]) ){//ha van ilyen db adat
        return $row[$fieldName];
    }
    return false;
}