<?php
//pl lotto nyerőosztályból általában 4 van
//5/90 esetén 2,3,4,5 találat
$talalatok = rand(0,5);

//az eseteket switchel kezeljük

switch($talalatok){
    case 2:
        echo "2 találat. A legkevesebb nyeremény.";
        break;
    case 3:
        echo "3 találat. Kicsit nagyobb nyeremény.";
        break;
    case 4:
        echo "4 találat. Nagy nyeremény.";
        break;
    case 5:
        echo "5 találat.Főnyeremény.";
        break;
    default:
        echo "nincs nyeremény.";
        break;
}
echo "<br>vége a switchnek. ($talalatok)";