<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
//lusta megoldás
for($i=1;$i<6;$i++){
    echo 'menü';
}
//jó és teljes megoldás
echo '<nav><ul>';
for($i=1;$i<=5;$i++){
    echo '<li><a href="#menu'.$i.'">menüpont '.$i.'</a></li>';
}
echo '</ul></nav>';




//2. Készítsünk programot, amely kiszámolja az első 100 darab. természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
$sum=0;
for($i=1;$i<=100;$i++){
    $sum+=$i;
}
echo '<h2>A számok összege 1-100ig: '.$sum.'</h2>';


// 3. Készítsünk programot, amely kiszámolja az első 7 darab. természetes szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, ..., 7 számokat.)
$mtpl=1;
for($i=1;$i<=7;$i++){
    $mtpl*=$i;
}
echo '<h2>A számok szorzata 1-7ig: '.$mtpl.'</h2>';

//4. Készítsünk programot, amely kiszámolja az első 100 darab. páros szám
//összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a
//ciklusváltozó kétszeresét - így megkapjuk a páros számokat. Ezeket
//hasonlóan adjuk össze, mint a 2. feladatban).
//a. megoldas
$sum=0;
$ptlan = 0;
//stopper start
$start = microtime(true);
for($i=1;$i<=100;$i++){
    if($i%2==0){
        $sum+=$i;
    }else{
        $ptlan += $i;
    }
}
$end = microtime(true);
$runtime1 = $end - $start;//ennyi idő telt el

echo "<h2>A páros számok összege 1-100 : $sum ($ptlan)</h2>";
//b. megoldás a javaslat alapján
$sum = 0;
$start = microtime(true);
for($i=1;$i<=50;$i++){
    $sum += $i*2;
}
$end = microtime(true);
$runtime2 = $end - $start;//ennyi idő telt el
echo "<h2>A páros számok összege 1-100 : $sum</h2>";

//var_dump($runtime1,$runtime2);//csak fejlesztés alatt!!!
echo '<pre>'.var_export($runtime1,true).'</pre>';
echo '<pre>'.var_export($runtime2,true).'</pre>';

//5. Készítsünk programot, amely kiszámolja az első 100 darab. páratlan
//szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban
//vegyük a ciklusváltozó kétszeresét eggyel csökkentve - így megkapjuk a
//páratlan számokat. Ezeket hasonlóan adjuk össze, mint az 2.
//feladatban).
$sum=0;
for($i=1;$i<=50;$i++){
    $sum += $i*2-1;
}
echo "<h2>A páratlan számok összege 1-100 : $sum</h2>";
