<?php
//tömbök kezelése
$tomb = [];//üres tömb létrehozása

$tomb = [123, true, 'szöveg', 6/9];//tomb megadása alap értékekkel automatiku tömb indexre

echo '<pre>'.var_export($tomb,true).'</pre>';
echo '<pre>';
var_dump($tomb);
echo '</pre>';

//tömb elemének kiírása
echo $tomb[0];
//tömb elemszáma
$elemszam = count($tomb);

//tömb bővítése automatikus indexre
$tomb[] = 'új elem';
echo '<pre>'.var_export($tomb,true).'</pre>';

//tömb bővítése irányított indexre
$tomb[100] = 'új elem';
echo '<pre>'.var_export($tomb,true).'</pre>';

//tömb bővítése irányított asszociativ indexre
$tomb['user_id'] = 11;
echo '<pre>'.var_export($tomb,true).'</pre>';

//Asszociatív tömb
$user = [
    'id' => 1,
    'username' => 'testuser',
    'email' => 'hgy@iworkshop.hu'
];
echo '<pre>'.var_export($user,true).'</pre>';
echo 'user email:'.$user['email'];