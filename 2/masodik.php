<?php
//pure PHP file
echo 'Helo PHP!';
$dobas = rand(1,6);
echo "<h2>A dobás eredménye: $dobas</h2>";
//dobjunk 5-ször és írjuk ki az összegüket.
//ciklus bemutatása
/*
for(ciklusváltozó kezdeti értéke;belépési feltétel vizsgálata;ciklusváltozó léptetése){
    //ciklusmag
}
 */
//összeg
$sum = 0;
for($i = 1 ; $i <= 5 ; $i = $i + 1){
    //ciklusmag
    echo "<br>Ciklusváltozó értéke: $i";
    //összeg növelése a dobás értékével (1-6)
    $sum = $sum + rand(1,6);
}
echo "<h2>A dobások összege: $sum</h2>";

//a összeg mellett szerepeljenek a dobások egyenkénti értékei is
// dobások: 1,4,2,3,6,
// a dobások összege: 16
//összeg
$sum = 0;//változó kiürítése, alapértékre állítása
echo 'Dobások: ';
for($i = 1 ; $i <= 5 ; $i++){ //operátor ++ -> egyel növelés (--)
    //ciklusmag
    $dobas = rand(1,6);//dobásérték eltárolása ideiglenesen
    echo "$dobas,";
    //összeg növelése a dobás eltárolt értékével
    $sum += $dobas;// operátor += -> $sum = $sum + $dobas => $sum += $dobas (-=, *=., /=)
}
//összeg kiírása
echo "<br>A dobások összege: $sum";
