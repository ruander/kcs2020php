<?php
//@todo: átnézni:  https://www.php.net/manual/en/ref.dir.php
//@todo: átnézni: fopen,fread ,fwrite,fclose - https://www.php.net/manual/en/ref.filesystem.php
//erőforrások beállítása
$dir = 'test/';//ebben a mappában szeretnénk majd dolgozni
//var_dump(is_dir($dir));//létezik-e a mappa
if(!is_dir($dir)){
    //ha nem létezik a mappa, készítsük el (mkdir())
    mkdir($dir,0755, true);
}

$fileName = 'myfile.data';//egy ilyen filet szeretnénk létrehozni
//tömb az adatokkal amelyet későbbi visszaolvasásra/feldolgozásra tárolni szeretnénk egy fileban
$data = [
  'id' => 1,
  'username' => 'superadmin',
  'email' => 'hgy@iworkshop.hu',
  'password' => md5('p4ssw0rd')
];

//teszt
echo '<pre>';
var_dump($data);
echo '</pre>';

//adattömb előkészítése tároláshoz (string)
//1. sorozat
$sorozat = serialize($data);
//teszt
echo '<pre>';
var_dump($sorozat);
echo '</pre>';
//ezen a ponton azadatok stringben vannak ezért egy fileba kiírhatóak
//file elkészítése komplex eljárással
//sorozat menjen az eredeti filenévbe
var_dump(file_put_contents($dir.$fileName,$sorozat));

//olvassuk vissza a filebol az eltárolt stringet, és tároljuk el egy változóban
$sorozatFile = file_get_contents($dir.$fileName);
//visszaalakítás
$sorozatTomb = unserialize($sorozatFile);
//teszt
echo '<pre>';
var_dump($sorozatTomb);
echo '</pre>';

//az eredeti és a sorozatból kapott tömb összehasonlítása (érték és típus!)
if( $data === $sorozatTomb ){
    echo 'A 2 tömb teljesen egyezik';
}

//2. CSV Comma Separated Values
$fileName = 'mycsv.csv';
$handle = fopen($dir.$fileName,'w');
fputcsv($handle,$data,';');
fclose($handle);

//adatok visszaolvasása, jelenleg nem tudjuk visszanyerni a kulcsokat csak az értékek kerültek tárolásra
$row = 1;
if (($handle = fopen($dir.$fileName, "r")) !== FALSE) {
    while (($data2 = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $num = count($data2);
        echo "<p> $num fields in line $row: <br></p>\n";

        for ($c=0; $c < $num; $c++) {
            echo $data2[$c] . "<br>\n";
        }
        $row++;
    }
    fclose($handle);
}


//3. json
$jsonData = json_encode($data);
//teszt
echo '<pre>';
var_dump($jsonData);
echo '</pre>';
$fileName = 'myjson.json';//filenév a json string tárolására

//file_put_contents($dir.$fileName,$jsonData.PHP_EOL,FILE_APPEND | LOCK_EX); //hozzáírja a filehoz egy új sorba a stringet
file_put_contents($dir.$fileName,$jsonData);//ez mindig ujraírja a filet
$jsonFile = file_get_contents($dir.$fileName);

//visszalakítás
$jsonTomb = json_decode($jsonFile,true);
//teszt
echo '<pre>';
var_dump($jsonTomb);
echo '</pre>';
//az eredeti és a sorozatból kapott tömb összehasonlítása (érték és típus!)
if( $data === $jsonTomb ){
    echo 'A 2 tömb teljesen egyezik<br>';
}else{
    echo 'A 2 tömb különbözik<br>';
}

//biztonságos(?) md5()
$pass = '123456';
//1.
$secret_key = 'S3cr3t_K3y!';
//elkóduljuk
echo $kodoltPass = md5($pass.$secret_key);

//2.
$kodoltPass = md5($pass);
for($i=0;$i<100;$i++) {
    $kodoltPass = md5($kodoltPass);
}
echo '<br>'.$kodoltPass;

//nem MD5, viszont biztonságos :)
$kodoltPass = password_hash($pass,PASSWORD_BCRYPT);
echo '<br>'.$kodoltPass;

//elkodolt hash és kapott jelszó egyezés ellenőrzése
$password_verify = password_verify('012345',$kodoltPass);
var_dump($password_verify);