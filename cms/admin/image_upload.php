<?php
//célmappa meghatározása ahova a kész képe(ke)t tesszük
$dir = "../public/uploads/";
if(!is_dir($dir)){//mappa ellenőrzése
    mkdir($dir,0755,true);
}

if(!empty($_POST)){
    $hiba = [];
    $validTypes = ['image/jpeg','image/jpg'/*, 'image/png*/];//ilyen formátumokat engedünk majd feltölteni
    //var_dump($_FILES);
    /*if(!in_array($_FILES['file']['type'],$validTypes)){
        die('nem jó tipus');
    }*/
    //file feltöltés siker esetén
   if($_FILES['file']['error'] === 0 && is_uploaded_file($_FILES['file']['tmp_name'])){
       //képtipus ellenőrzése
       $info = @getimagesize($_FILES['file']['tmp_name']);
       //var_dump($info);
       if($info !== false){
            //képtipus jó e
           if(!in_array($info['mime'],$validTypes)){
               $hiba['file']='<span class="error">Nem megfelelő képformátum!</span>';
           }else{
               //képtipus jó
                //most 150x150 négyzet thumbnail vágunk ki (crop)
               $dst_w = $dst_h = 150;
                //vászon a kívánt mérettel
               $dst_image = imagecreatetruecolor($dst_w,$dst_h);
               //vászon koordinátái
               $dst_x = 0;
               $dst_y = 0;

               //forrás koordinátái
               $src_x = 0;
               $src_y = 0;
               //forrás méretei
               $src_w = $info[0];
               $src_h = $info[1];
               //képarány segéd
               $ratio = $src_w/$src_h;//>1 fekvő, < 1 álló
               if($ratio>1){
                   //fekvő
                   //eltolás x-tengelyen a kicsínyített méretkülönbség felével
                   $target_h = $dst_h;
                   $target_w = $dst_w*$ratio;
                   $dst_x = round(($target_w-$dst_w)/2);
                   $dst_y=0;
               }else{
                   //álló
                   $target_w = $dst_w;
                   $target_h = $dst_h/$ratio;
                   $dst_x = 0;
                   $dst_y = round(($target_h - $dst_h)/2);
               }
                   //forrás memóriába
               $src_image = imagecreatefromjpeg($_FILES['file']['tmp_name']);
               //kép átalakítása a memóriában méretezéssel, pixel rekalkulációval
               imagecopyresampled (  $dst_image ,  $src_image ,  -$dst_x ,  -$dst_y ,  $src_x ,  $src_y ,  $target_w ,  $target_h ,  $src_w ,  $src_h );
               //ez a file látszódjon képnek
               //header('content-type:image/jpeg');
               $fileName = $dir.'thumb-'.$_FILES['file']['name'];//legyen az eredeti neve egy thumb- előtaggal
                //kép kiírása a fileba
               imagejpeg($dst_image,$fileName,100);

               //takarítás a memóriából a művelet után
               imagedestroy($dst_image);
               imagedestroy($src_image);
           }
       }else{
           $hiba['file']='<span class="error">Nem képet töltöttél fel!</span>';
       }
   }
    if(empty($hiba)){

    }
}
?>
<form method="post" enctype="multipart/form-data">
    <label>
        <input type="file" name="file">
        <?php
        //hiba kiírás ha van
        if(isset($hiba['file'])){
            echo $hiba['file'];
        }
        ?>
    </label>
    <button name="submit">Feltöltés</button>
</form>
