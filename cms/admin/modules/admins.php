<?php
//erősforrások
//önálló megnyitás elleni védelem
if(!isset($auth)){
    header("location:index.php");
    exit();
}
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);//cél ID amivel műveletet végzünk
$action = filter_input(INPUT_GET, 'action');//szükséges művelet urlből
$output = ''; //ide gyűjtjük a kiírandó elemeket
if (!empty($_POST)) {
    //hibakezelés
    $hiba = [];
    //username min 3 kar
    $username = trim(filter_input(INPUT_POST, 'username'));
    //szövegvégi spacek eltávolításával
    if (mb_strlen($username, 'utf-8') < 3) {
        $hiba['username'] = '<span class="error">Kötelező kitölteni (min 3 karakter)!</span>';
    }
    //email - legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Érvénytelen adat!</span>';
    } else {
        //foglalt-e már a db-ben
        $qry = "SELECT id FROM admins WHERE email  = '$email' LIMIT 1 ";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row_email = mysqli_fetch_row($result);//a [0] van az id, ha van, de most már az is baj ha egyáltalán van row
        //var_dump($row_email,$tid);
        if ($row_email AND $row_email[0] != $tid) {
            $hiba['email'] = '<span class="error">Foglalt email cím!</span>';
        }
    }
    //jelszó min 6 karakter kell legyen
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if(strlen($pass)>0 OR $action=='create') {
        if (mb_strlen($pass, "utf-8") < 6) {
            $hiba['pass'] = '<span class="error">Érvénytelen adat (min 6 karakter) !</span>';
        } elseif ($pass != $repass) { //jelszó újra meg kell egyezzen a jelszóval
            $hiba['repass'] = '<span class="error">Érvénytelen adat (a jelszavak nem egyeztek) !</span>';
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            //password_hash visszafejtése: password_verify(jelszo,tárolt hash)
        }
    }
    //status
    $status = filter_input(INPUT_POST, 'status',FILTER_VALIDATE_INT)?:0;

    if (empty($hiba)) {
        //adatok 'tisztázása'
        $data = [
            'username' => mysqli_real_escape_string($link,$username),
            'email' => $email,
            'status' => $status
        ];
        //ha üres a $pass akkor nem, de ha nem üres, akkor benne kell legyen a tömbben
        if($pass) $data['pass']=$pass;
        //dátumidő uj és módosítás esetben
        if($action == 'create'){
            $data['time_created'] = date('Y-m-d H:i:s');
            //insert query összeállítása
            $fieldNames = '`' . implode('`,`', array_keys($data)) . '`';
            $values = "'" . implode("','", $data) . "'";
            $qry = "INSERT INTO 
                    `admins`( $fieldNames ) 
                    VALUES ( $values )";
        }else{//update
            $data['time_updated'] = date('Y-m-d H:i:s');
            //query összeállítása
            $upd = [];
            foreach($data as $field => $v){
                $upd[] = "`$field` = '$v'";
            }
            $update_set = implode(',',$upd);
            $qry = "UPDATE `admins` 
                    SET 
                    $update_set
                WHERE `id` = $tid
                LIMIT 1";
        }
        //var_dump($data);
        mysqli_query($link, $qry) or die(mysqli_error($link));
        header('location:'.$baseURL);//index fileban van összeállítva
        exit();
    }
}

//switch alkalmazása az action-ök szétválasztására
switch ($action) {

    case "delete":
        if ($tid) {
            mysqli_query($link, "DELETE FROM admins WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
            //hogy ne maradjon url paraméter
            header('location:' . $baseURL);
            exit();
        } else {
            die("Ne piszkáld az urlt!");
        }
        break;

    case "update":
        if ($tid) {
            $qry = "SELECT * FROM admins WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
            //var_dump($row);
        }
        $formTitle = "Módosítás --$tid--";
        $formSubmit = "Módosítom";
            //update form
        //break;

    case "create":
        $row = isset($row)?$row:[];
        $formTitle = isset($formTitle)?$formTitle:"Új felvitel";//ha nincs, adunk, ha már van, marad...
        $formSubmit = isset($formSubmit)?$formSubmit:"Új felvitel";
        //űrlap
        $form = '<form method="post" class="registration">
        <h1>'.$formTitle.'</h1>';
        //username
        $form .= '<label>
            <span>Név<sup>*</sup></span>
            <input
                    type="text"
                    name="username"
                    placeholder="John Doe"
                    value="' . getValue('username',$row) . '">'
            . hibaKiir('username') . '</label>';
        //email
        $form .= '<label>
            <span>Email<sup>*</sup></span>
            <input
                    type="text"
                    name="email"
                    placeholder="your@email.com"
                    value="' . getValue('email',$row) . '">' .
            hibaKiir('email') . '</label>';
        //pass
        $form .= '<label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="pass" placeholder="******" value="">' . hibaKiir('pass') . '</label>';
        //repass
        $form .= '<label>
            <span>Jelszó újra<sup>*</sup></span>
            <input type="password" name="repass" placeholder="******" value="">' . hibaKiir('repass') . '</label>';
        //status
        $form .= '<label class="status">
            <input type="checkbox" name="status"
                   value="1" ' . ((filter_input(INPUT_POST,'status')
                OR
                isset($row['status']) AND $row['status'] == 1 AND empty($_POST))?'checked':'') . '>  aktív?</label>';
        //submit
        $form .= '<button>'.$formSubmit.'</button>
    </form>';
        //hozzátesszük az űrlapot az outputhoz
        $output .= $form;
        break;

    default://listázás (read)
        //admin sorok lekérése
        $qry = "SELECT id, username,email,lastlogin,status FROM admins";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
//táblázat elkészítése | az baseUrl az index file erőforrása ahova a moduleokat includeoljuk
        $table = '<a href="'.$baseURL.'&amp;action=create">Új felvitel</a><table border="1">';

        while ($row = mysqli_fetch_assoc($result)) {
            $table .= '<tr>';
            $table .= '<td>' . $row['id'] . '</td>';
            $table .= '<td>' . $row['username'] . '</td>';
            $table .= '<td>' . $row['email'] . '</td>';
            $table .= '<td>' . $row['lastlogin'] . '</td>';
            $table .= '<td>' . $row['status'] . '</td>';
            //crud
            $table .= '<td><a href="'.$baseURL.'&amp;action=update&amp;id=' . $row['id'] . '">edit</a> | <a href="'.$baseURL.'&amp;action=delete&amp;id=' . $row['id'] . '" onclick="return confirm(\'Are you sure to delete?\')">delete</a></td>';
            $table .= '</tr>';
        }
        $table .= '</table>';
        //hozzátesszük a lista táblát az outputhoz
        $output .= $table;
}
//vége a switchnek


