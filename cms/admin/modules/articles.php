<?php
//@todo, kép feltöltés lehetősége public/images/articles/{cikkid}/...

//védelem önálló futtatás ellen
if (!$link) {
    header('location:index.php');
    exit();
}
//Erőforrások
$output = '';//ide gyűjtjük a kimentre írandó elemeket
//action az urlből
$act = filter_input(INPUT_GET, 'action') ?: 'list';//rövid shorten if az igaz ág a condition maga
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//ha kapunk számot akkor legyen az, ha nem akkor legyen null
//var_dump($act);
$dbTable = 'articles';
//var_dump($_POST);

//űrlap kezelése
if (!empty($_POST)) {
    $hiba = [];
    //cím kötelező
    //title
    $title = filter_input(INPUT_POST, 'title');
    $title = strip_tags($title);//távolítsuk el az esetleg TAGeket
    $title = mysqli_real_escape_string($link, $title);//mysqli injection elleni védelem
    if ($title == '') {
        $hiba['title'] = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>Adj meg címet!</div>';
    } else {
        //seo_title itt mindig a cimből készítjük a példában
        $seo_title = ekezettelenit($title);
    }

    //lead
    $lead = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'lead'));
    if (mb_strlen($lead, 'utf-8') > 400) {
        $hiba['lead'] = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>Max 400 karakter lehet a vezér!</div>';
    }
    //content
    $content = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'content'));
    //author
    $author = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'author'));

    //time_published
    $time_published = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'time_published'));

    //echo '<pre>' . var_export($hiba, true) . '</pre>';
    if (empty($hiba)) {
        $now = date('Y-m-d H:i:s');
        $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT) ?: 0;
        //adatok rendberakása egy könnyen kezelhető tömbbe
        $data = [
            'title' => $title,
            'seo_title' => $seo_title,
            'lead' => $lead,
            'content' => $content,
            'author' => $author,
            'time_published' => $time_published,
            'status' => $status,
        ];
        //qry elkészítése uj és update esetre

        if ($act == 'create') {
            $data['time_created'] = $now;
            $qry = "INSERT INTO $dbTable(`" . implode('`,`', array_keys($data)) . "`)
                VALUES('" . implode("','", $data) . "')";//kérés összeállítása
        } else {
            //update esetén a data tömb a time updated kell tartalmazza  a time_createdet nem
            $data['time_updated'] = $now;
            //az update mezőket ciklusban állítjuk össze
            $fields = '';
            $fieldElements = [];
            foreach ($data as $fieldName => $fieldData) {
                $fieldElements[] = "$fieldName = '$fieldData'";
            }
            $fields = implode(',', $fieldElements);//az elemeket implodoljuk egy , -vel

            $qry = "UPDATE $dbTable
                    SET
                    $fields      
                    WHERE id ='$tid' LIMIT 1";
        }

        //kérés futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));

        //átirányítás listázásra
        header('location:' . $baseURL);
        exit();
    }
}

//CRUD működés

switch ($act) {
    case 'delete':
        //echo 'törlünk: ' . $tid;
        //ha mit törölni, törlünk
        if ($tid) {
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = '$tid' LIMIT 1") or die(mysqli_error($link));
            //átirányítunk listázásra
            header('location:' . $baseURL);
            exit();
        }
        break;
    case 'update':

        //adatok lekérése

        if ($tid) {
            $qry = "SELECT * FROM $dbTable WHERE id = '$tid' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
            //echo '<pre>'.var_export($row,true).'</pre>';
        }
        //űrlapcím
        $formTitle = "Cikk módosítása --{$row['id']} --";
    //break;
    case 'create':
        //Űrlap új felvitelhez és módosításhoz
        $row = isset($row) ? $row : [];//ha nem lenne adat akkor legyen üres $row tömb
        if (!isset($formTitle)) $formTitle = 'Új felvitel';
        $form = '<div><a href="' . $baseURL . '&amp;action=list">&lt;-- vissza</a></div>
                <h1>' . $formTitle . '</h1>
                <form method="post">';//visszagomb , form nyitás, cím
        //title
        $form .= '<label>
        Cím<sup>*</sup> <input type="text" name="title" value="';
        $form .= getValue('title', $row);//mezőérték visszaírása
        $form .= '" placeholder="Cikk címe">';
        //mezőhiba
        $form .= hibaKiir('title');
        $form .= '</label>';
        //seo title
        $form .= '<br><label>
        Seo cím<input type="text" name="seo_title" value="';
        $form .= getValue('seo_title', $row);//mezőérték visszaírása
        $form .= '" disabled>';
        $form .= '</label>';
        //lead
        $form .= '<br><label>
        Vezér (begépelt karakterek száma)<br><textarea name="lead" rows="5" cols="80" placeholder="max 400 karakteres bevezető szöveg...">' . getValue('lead', $row) . '</textarea>';
        //mezőhiba @todo, a túllógó szöveg színe legyen piros (400+) VAGY javascriptes karakterszámláló beépítése
        $form .= hibaKiir('lead');
        $form .= '</label>';
        //content
        $form .= '
          <div>
            <label for="content">Tartalom</label>
            <div class="box-body pad">     
                <textarea class="textarea" id="content" name="content" placeholder="Cikk cikk tartalma..."
                          style="width: 100%; height: 450px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">' . getValue('content', $row) . '</textarea>
              
            </div>
          </div>';
        //author
        $author = getValue('author', $row) ?: $_SESSION['userdata']['username'];//ha nincs akkor legyen a username aki be van lépve
        $form .= '<br><label>
        Szerző <input type="text" name="author" value="';
        $form .= $author;//mezőérték visszaírása
        $form .= '" placeholder="szerző">';
        $form .= '</label>';
        //time_published @todo legyen botstrappes datetime selector
        $now = date('Y-m-d H:i:s');
        $time_published = getValue('time_published', $row) ?: $now;
        $form .= '<div>
                        <label for="datepicker">Megjelenés ideje: </label>
                        <div class="input-group date" data-children-count="1">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar" data-children-count="0"></i>
                      </div>
                  <input type="text" name="time_published" class="form-control pull-right" id="datepicker" value="';
        $form .= $time_published;//mezőérték visszaírása
        $form .= '" placeholder="' . $now . '">';
        $form .= '</div></div>';

        //státusz
        //ha volt post és nincs a tömbjében akkor tuti nem kell kipipálni
        //@todo ezt egyszerűsíteniˇˇˇˇˇˇ
        if (!empty($_POST)) {
            if (filter_input(INPUT_POST, 'status') == false) {
                $checked = '';
            } else {
                $checked = 'checked';
            }
        } else {
            if (isset($row['status']) and $row['status'] == 1) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
        }
        //^^^^^^^^^^^^

        $form .= '<br><label>
    Státusz <input type="checkbox" name="status" value="1" ' . $checked . '></label>';

        $form .= '<br><button>Mehet</button>
                </form>';//submit és form zárás
        $output .= $form;
        break;
    default://nincs vagy ismeretlen action paraméter, lista
        //@todo: törlés confirm megvalósítása javascripttel
        //echo 'lista';
        $qry = "SELECT * FROM $dbTable";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<div class="row">
                    <div class="col-2 offset-10">
                        <a class="btn btn-block btn-primary" href="' . $baseURL . '&amp;action=create">Új felvitel</a>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-12 pt-2">
                    <table class="table table-striped table-responsive-sm">
                     <tr>
                      <th>Azonosító</th>
                      <th>Cím</th>
                      <th>Szerző</th>
                      <th>Státusz</th>
                      <th>Megjelenés</th>
                      <th>Művelet</th>
                    </tr>';
        while (null !== $row = mysqli_fetch_assoc($result)) {
            //sorok
            $table .= '<tr>
                          <td>' . $row['id'] . '</td>
                          <td title="' . $row['seo_title'] . '">' . $row['title'] . '</td>
                          <td>' . $row['author'] . '</td>
                          <td>' . $row['status'] . '</td>
                          <td>' . $row['time_published'] . '</td>
                          <td><a class="btn btn-warning btn-xs" href="' . $baseURL . '&amp;action=update&amp;tid=' . $row['id'] . '">Módosít</a> <a class="btn btn-danger btn-xs" href="' . $baseURL . '&amp;action=delete&amp;tid=' . $row['id'] . '">Töröl</a></td>
                        </tr>';
        }
        $table .= '</table>
                </div>
               </div>';
        $output .= $table;
        break;
}
//a modulhoz mindenképp töltődik
if ($act == 'create' || $act == 'update') {
//stílusok a modulhoz
    $additional_styles .= '<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
                        <link rel="stylesheet" href="css/bootstrap3-wysihtml5.min.css">';
//scriptek a modulhoz
    $additional_scripts .= '
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/locales/bootstrap-datetimepicker.hu.js" charset="UTF-8"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="js/bootstrap3-wysihtml5.all.min.js"></script>';
//működtető script a modulhoz
    $additional_scripts .= "<script>
    $(document).ready(function () {
        //Date picker
        $('#datepicker').datetimepicker({
            language: 'hu',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        })
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>";
}