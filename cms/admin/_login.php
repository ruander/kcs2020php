<?php
require "../config/connect.php";//db csatlakozás
require "../config/settings.php";//erőforrások
require_once "../config/functions.php";//saját eljárások
//munkafolyamat indítása:
session_start();
//var_dump($_SESSION);
//unset($_SESSION['teszt']);//$_SESSION['teszt']='Teszt adat.';
//echo session_id();//mf azonosító 2ro22d3g7i3kbon6es2n6atb0t
//ha már be van lépve mit keres itt?
if (auth()) {
    header("location:index.php");
    exit();
}
$msg = 'Kérjük írja be belépési adatait:';//kiírandó üzenet
if (!empty($_POST)) {
    if (login()) {
        header("location:index.php");
        exit();
    } else {
        $msg = 'Nem megfelelő email/jelszó páros...';
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztráció - belépés</title>
</head>
<body>
<form method="post">
    <div>
        <?php echo $msg; ?>
    </div>
    <label>
        email <input type="text" name="email" id="email" placeholder="email@cim.hu"
                     value="<?php echo filter_input(INPUT_POST, 'email'); ?>">
    </label>
    <label>
        jelszó <input type="password" name="password" id="password" value="">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>