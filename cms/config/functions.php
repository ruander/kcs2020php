<?php
/*saját eljárások gyüjteménye*/
/**
 * Admin menü összeállítása
 * @param array $adminMenu
 * @return string
 */
function adminMenu($adminMenu = [])
{
    $menu = '<ul class="sidebar-menu" data-widget="tree">
            <li class="header">Főmenü</li>';
//menüpontok
    foreach ($adminMenu as $menuId => $menuItem) {
        $menu .= '<li>
        <a href="?p=' . $menuId . '">
            <i class="' . $menuItem['icon'] . '"></i> <span>' . $menuItem['title'] . '</span>
            <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">Hot</small>-->
            </span>
        </a>
    </li>';
    }
    $menu .= '</ul>';
    return $menu;
}


/**
 * Beléptető eljárás a login űrlaphoz
 * @return bool
 */
function login(){
    global $link, $secret_key;//ezek máshonnan jönnek, az eljárásnak látnia kell

    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $password = filter_input(INPUT_POST, 'password');
    if ($email) {
        $qry = "SELECT id,pass,username FROM admins WHERE email ='$email' AND status = 1 LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $admin = mysqli_fetch_assoc($result);
        //var_dump($admin);
        //jelszó helyesség ellenőrzése
        $passCheck = isset($admin['pass'])?password_verify($password, $admin['pass']):false;//ha nem találtunk admint, legyen false
        if ($passCheck) {
            //adminból kivesszük a jelszót és beletesszük az emailt
            unset($admin['pass']);
            $admin['email'] = $email;
            //eltároljuk a sessionben az admin adatokat, hogy meglegyen
            $_SESSION['userdata'] = $admin;
            $_SESSION['id'] = session_id();
            //sikeres ellenőrzés
            $sid = session_id();
            $stime = time();//timestamp (int)
            $spass = md5($admin['id'] . $sid . $secret_key);
            //var_dump($sid,$stime,$spass);

            //beragadt belépések törlése
            mysqli_query($link,"DELETE FROM sessions WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
            //belépés tárolása
            $qry = "INSERT INTO sessions(sid,stime,spass)
                                VALUES('$sid','$stime','$spass')";
            mysqli_query($link, $qry) or die(mysqli_error($link));
            return true;//minden ok
        }
    }
    return false;//valami nem ok
}

/**
 * Belépés érvényességének ellenőrzése
 * @return bool
 */
function auth()
{
    global $link, $secret_key;
    $sid = session_id();//aktuális mf id
    $now = time();//mostani idő
    $expired = $now - 15 * 60;//most 15p
    //töröljük a lejárt folyamatokat (takarítás)
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired ") or die(mysqli_error($link));
//lekérjük van-e ilyen sessionunk
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    //var_dump($row);
//gyártsuk le a mostani adatokból a jelszót
    if(!isset($_SESSION["userdata"]["id"])) return false;//ha nincs userdata notice javítása
    $spass = md5($_SESSION["userdata"]["id"] . $sid . $secret_key);
    if (!empty($row) && $spass === $row[0]) {
        //stime frissítése
        mysqli_query($link,"UPDATE sessions SET stime = '$now' WHERE sid = '$sid'") or die(mysqli_error($link));
        return true;
    }
    return false;
}

/**
 * Kiléptetés
 * :void
 */
function logout(){
    global $link;
    //roncsoljunk
    mysqli_query($link, "DELETE FROM sessions WHERE sid = '{$_SESSION['id']}'") or die(mysqli_error($link));
    $_SESSION = [];
}

/**
 * Saját hiba kiíró eljárás
 * a $hiba változóban tárolt hibák kiírására
 * @param $inputName
 * @return bool
 */
function hibaKiir($inputName)
{
    global $hiba;//az eljárás idejére elérhetővé tesszük a hibatömböt

    if (isset($hiba[$inputName])) {
        return $hiba[$inputName];
    }
    return false;
}

/**
 * Mezőértékek visszaadása mezőnév alapján + ha van adatbázis row akkor az az adat tér vissza vagy semmi
 * @param $fieldName :string | a mező neve
 * @param  $row :array | a lekért adatok asszociatív tömbje az adatbázisból
 * @return string - beírandó érték a mezőbe
 */
function getValue($fieldName, $row = [])
{
    $ret = filter_input(INPUT_POST, $fieldName);
    if($ret !== NULL ){//ha találtunk ilyen elemet (akár üresen is)
        return $ret;
    }elseif( isset($row[$fieldName]) ){//ha van ilyen db adat
        return $row[$fieldName];
    }
    return false;
}
/*
 * string kezelő eljárások
 */
/**
 * @param $str
 * @return string
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

/**
 * @param $str
 * @return string
 */
function ekezettelenitFileName($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}