<?php
$blog ='<section class="greyBg" id="blog">
    <div class="wrapper">
        <h2 class="subheading orange-text">blog</h2>
        <h1 class="dark-text">whats\'s news</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod reiciendis
            veritatis. Ad illo odit officia?</p>
        <div class="grid-holder posts grid-3">';

 //lekérés adatbázisból

$qry = "SELECT id,title,seo_title,lead,author,time_published 
            FROM articles 
            WHERE
            status = 1
            AND time_published < '".date('Y-m-d H:i:s')."'
            ORDER BY time_published DESC
            LIMIT 3";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
while($row = mysqli_fetch_assoc($result)){
    //random szám a kommenteknek, illetve  képek random elemének
    $randomNumber = rand(50,100);
    //dátum csak éééé-hh-nn formátumban kell
    $datum = date('Y-m-d',strtotime($row['time_published']));
    $blog .='<article class="post">
                <header>
                    <div class="comments">'.$randomNumber.'</div>
                    <img src="https://picsum.photos/640/360?random='.$randomNumber.'" alt="kép 1"></header>
                <h2><a href="#'.$row['seo_title'].'" class="post-title">'.$row['title'].'</a></h2>
                <p>'.$row['lead'].'</p>
                <footer>
                    <div class="author"><img alt="'.$row['author'].'" src="https://picsum.photos/40?random='.$randomNumber.'"> írta: '.$row['author'].'</div>
                    <time datetime="'.$datum.'">'.$datum.'</time>
                </footer>
            </article>';
}
 $blog .='</div>
    </div>
</section>';

//blog kiírása
echo $blog;