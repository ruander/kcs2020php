<?php
require "../config/connect.php";//db csatlakozás
require "../config/settings.php";//erőforrások
require_once "../config/functions.php";//saját eljárások
?><!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HTML portfolio - onepager</title>
    <!--fontawesome 4 ikonkészlet betöltése-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="css/owl.carousel.css">&lt;!&ndash;owl carousel stilusai&ndash;&gt;
    <link rel="stylesheet" href="css/cd-style.css">&lt;!&ndash; szövegváltó stílusai &ndash;&gt;
    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link rel="shortcut icon" href="images/apple-touch-icon.png">
    <style>

    </style>

</head>
<body>
<nav id="main" class="sticky">
    <div class="wrapper">
        <ul class="mainmenu">
            <li class="logo"><a href="#"><img src="images/ruander-logo.png" alt="Oldal logo"></a><span
                    id="menuToggle"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
            <li><a href="index.html" class="active">HOME</a></li>
            <li><a href="#about">about</a></li>
            <li><a href="#skills">SKILL</a></li>
            <li><a href="#services">services</a></li>
            <li><a href="#projects">PORTFOLIO</a></li>
            <li><a href="#testimonial">TESTIMONIAL</a></li>
            <li><a href="#blog">BLOG</a></li>
            <li><a href="#contact">CONTACT</a></li>
        </ul>
    </div>
</nav>
<section id="slider">
    <div class="wrapper" id="hero">
        <!--h2{I am George Horvath}+(h1>{I am a}+span.anim{sitebuilder})+(p>lorem20)+a.btn[href=#]{download cv}-->
        <h2 class="animate__animated animate__jello">I am George Horvath</h2>
        <h1 class="cd-headline clip">
            I am a
            <span class="cd-words-wrapper">
				<b class="is-visible">developer</b>
				<b>designer</b>
				<b>hacker</b>
                <b>pizza fan</b>
			</span>
        </h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis esse iusto laboriosam laborum maxime
            mollitia nostrum omnis ullam vitae voluptatibus.</p>
        <a href="#" class="btn type-1">download cv</a>
    </div>
</section>
<section class="wrapper" id="about">
    <h2 class="subheading orange-text">about</h2>
    <h1 class="dark-text">about myself</h1>
    <p class="dark-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod
        reiciendis
        veritatis. Ad illo odit officia?</p>
    <div class="grid-holder grid-2-1 text-left">
        <div class="box">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium eveniet exercitationem hic
                inventore itaque possimus provident sit tempora totam, voluptatem. Cumque eos iure maiores odio officiis
                quae
                suscipit? Alias consequatur esse, iure rem repellendus saepe tempora totam ullam voluptates. Cum
                delectus,
                dolore doloribus excepturi harum illo ipsam labore libero minima molestiae necessitatibus odit
                perspiciatis
                possimus quae recusandae sequi voluptatibus? Beatae earum eos, et maiores odit provident rem sit
                veritatis
                voluptas? Consectetur impedit iusto quam quis recusandae. Laudantium minima pariatur vitae?</p>
            <!--ul.columned>li*6>{param $}+span{value $}-->
            <ul class="columned list-1">
                <li>param 1 : <span>value 1</span></li>
                <li>param 2 : <span>value 2</span></li>
                <li>param 3 : <span>value 3</span></li>
                <li>param 4 : <span>value 4</span></li>
                <li>param 5 : <span>value 5</span></li>
                <li>param 6 : <span>value 6</span></li>
            </ul>
        </div>
        <div class="box"><img src="images/play.png" alt="play"></div>
    </div>
</section>
<section class="greyBg" id="skills">
    <div class="wrapper">
        <h2 class="subheading orange-text">skill</h2>
        <h1 class="dark-text">design skill</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod reiciendis
            veritatis. Ad illo odit officia?</p>
        <div class="grid-holder grid-1-1 text-left">
            <div class="box"><h3>Some talk about my professional design skill</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At consectetur iure nobis ullam.
                    Consequatur
                    deserunt dolor error ex ipsum iure necessitatibus vitae. Ab accusantium animi distinctio eos facere,
                    iste maiores nam nulla praesentium sint voluptas!</p>
                <p>Accusantium adipisci aliquam aspernatur assumenda beatae commodi consectetur eligendi est eveniet,
                    fugiat
                    fugit hic illo inventore ipsum iure magnam magni molestias non obcaecati officiis quam quasi quidem
                    quisquam ratione saepe tempora ullam vitae voluptatem voluptates?</p>
            </div>
            <div class="box">
                <ul class="progress-bars">
                    <li class="bar">
                        <label><span class="title">html</span><span class="percentage">90%</span></label>
                        <progress class="percent-bar" value="90" max="100"></progress>
                    </li>
                    <li class="bar">
                        <label><span class="title">CSS</span><span class="percentage">95%</span></label>
                        <progress class="percent-bar" value="95" max="100"></progress>
                    </li>
                    <li class="bar">
                        <label><span class="title">jquery</span><span class="percentage">85%</span></label>
                        <progress class="percent-bar" value="85" max="100"></progress>
                    </li>
                    <li class="bar">
                        <label><span class="title">php</span><span class="percentage">90%</span></label>
                        <progress class="percent-bar" value="90" max="100"></progress>
                    </li>
                    <li class="bar">
                        <label><span class="title">wordpress</span><span class="percentage">90%</span></label>
                        <progress class="percent-bar" value="90" max="100"></progress>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <footer id="skill-counters">
        <div class="wrapper">
            <ul class="counters">
                <li>
                    <i class="fa fa-user"></i>
                    <span class="counter"><span class="value">7</span>+</span>
                    <p>Years of experience</p>
                </li>
                <li>
                    <i class="fa fa-calendar-check-o"></i>
                    <span class="counter"><span class="value">145</span>+</span>
                    <p>projects done</p>
                </li>
                <li>
                    <i class="fa fa-smile-o"></i>
                    <span class="counter"><span class="value">137</span>+</span>
                    <p>Happy clients</p>
                </li>
            </ul>
        </div>
    </footer>
</section>
<section id="services" class="wrapper">
    <h2 class="subheading orange-text">services</h2>
    <h1 class="dark-text">what i do</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod reiciendis
        veritatis. Ad illo odit officia?</p>
    <div class="grid-holder grid-3">
        <!--.card*6>(.head>span{$$.})+h2{web design}+i.icon.fa.fa-paint-brush+p>lorem25-->
        <div class="card">
            <div class="head"><span>01.</span></div>
            <h2>web design</h2>
            <i class="icon fa fa-desktop"></i>
            <p class="dark-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci earum facilis,
                fugiat nemo nobis
                ratione sapiente similique. Cum dolore esse ipsa quisquam sed. Non, sint!</p>
        </div>
        <div class="card">
            <div class="head"><span>02.</span></div>
            <h2>web development</h2>
            <i class="icon fa fa-code"></i>
            <p class="dark-text">Aliquam, aut dicta distinctio dolorem, doloribus labore minima neque nihil officiis
                quis quo quos
                repellendus sint. Assumenda, aut beatae explicabo neque omnis perferendis saepe temporibus.</p>
        </div>
        <div class="card">
            <div class="head"><span>03.</span></div>
            <h2>wordpress</h2>
            <i class="icon fa fa-wordpress"></i>
            <p class="dark-text">A aut, corporis dolorem eligendi nihil obcaecati. Cumque enim tenetur veritatis
                voluptate? Aliquid at aut
                cumque iure, labore necessitatibus nostrum, quidem quos rem, tempora voluptatem!</p>
        </div>
        <div class="card">
            <div class="head"><span>04.</span></div>
            <h2>graphic design</h2>
            <i class="icon fa fa-paint-brush"></i>
            <p class="dark-text">Architecto assumenda commodi corporis cumque, debitis dolorum eligendi fugiat incidunt
                inventore iusto
                laboriosam magnam placeat praesentium quam quisquam, quod recusandae repellat similique suscipit vel
                vitae.</p>
        </div>
        <div class="card">
            <div class="head"><span>05.</span></div>
            <h2>branding</h2>
            <i class="icon fa fa-crosshairs"></i>
            <p class="dark-text">Adipisci error expedita harum molestiae repellendus repudiandae vel voluptate. Aliquam,
                eius, esse ex
                explicabo fuga itaque, iusto libero maiores nobis reiciendis soluta ullam unde vero.</p>
        </div>
        <div class="card">
            <div class="head"><span>06.</span></div>
            <h2>opencart</h2>
            <i class="icon fa fa-shopping-cart"></i>
            <p class="dark-text">Aliquid blanditiis eligendi expedita facere, inventore ipsum magnam odit quam qui quis
                quod tempore,
                veritatis voluptate. Aperiam doloremque explicabo hic iure nihil quidem, repudiandae voluptate.</p>
        </div>
    </div>
</section>
<section id="projects" class="greyBg">
    <div class="wrapper">
        <h2 class="subheading orange-text">portfolio</h2>
        <h1 class="dark-text">recent projects</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod reiciendis
            veritatis. Ad illo odit officia?</p>
        <div class="portfolio-grid">
            <div id="myBtnContainer">
                <button class="btn active" onclick="filterSelection('all')"> Show all</button>
                <button class="btn" onclick="filterSelection('webDesign')"> Web design</button>
                <button class="btn" onclick="filterSelection('webDevelopment')"> Web development</button>
                <button class="btn" onclick="filterSelection('wordpress')"> Wordpress</button>
            </div>

            <!-- Portfolio Gallery Grid -->
            <div class="row">
                <div class="column webDesign">
                    <div class="content">
                        <div class="image"><img src="https://picsum.photos/640/480?a" alt="Mountains" style="width:100%"></div>
                        <h4>Mountains</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <div class="column webDesign">
                    <div class="content">
                        <div class="image"><img src="https://picsum.photos/640/480?b" alt="Lights" style="width:100%"></div>
                        <h4>Lights</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <div class="column webDesign">
                    <div class="content">
                        <div class="image"><img src="https://picsum.photos/640/480?c" alt="Nature" style="width:100%"></div>
                        <h4>Forest</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>

                <div class="column webDevelopment">
                    <div class="content">
                         <div class="image"><img src="https://picsum.photos/640/480?d" alt="Car" style="width:100%">
                         </div>                      <h4>Retro</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <div class="column webDevelopment">
                    <div class="content">
                         <div class="image"><img src="https://picsum.photos/640/480?e" alt="Car" style="width:100%">
                         </div>                      <h4>Fast</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <div class="column webDevelopment">
                    <div class="content">
                         <div class="image"><img src="https://picsum.photos/640/480?f" alt="Car" style="width:100%">
                         </div>                      <h4>Classic</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>

                <div class="column wordpress">
                    <div class="content">
                        <div class="image"><img src="https://picsum.photos/640/480?g" alt="People" style="width:100%"></div>
                        <h4>Girl</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <div class="column wordpress">
                    <div class="content">
                        <div class="image"><img src="https://picsum.photos/640/480?h" alt="People" style="width:100%"></div>
                        <h4>Man</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <div class="column wordpress">
                    <div class="content">
                        <div class="image"><img src="https://picsum.photos/640/480?i" alt="People" style="width:100%"></div>
                        <h4>Woman</h4>
                        <p>Lorem ipsum dolor..</p>
                    </div>
                </div>
                <!-- END GRID -->
            </div>
        </div>
    </div>
</section>
<section id="testimonials" class="wrapper">
    <h2 class="subheading orange-text">testimonial</h2>
    <h1 class="dark-text">our clients saying</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod reiciendis
        veritatis. Ad illo odit officia?</p>
    <div class="owl-carousel owl-theme">
        <div class="item">
            <div class="holder">
                <div class="card"><img src="https://picsum.photos/100/100?1" alt="kep 1">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <span class="name">Lorem ipsum</span></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos esse expedita id iste necessitatibus,
                    quos
                    repudiandae. Animi atque nesciunt repellat sequi. Asperiores excepturi illo natus?</p>
            </div>
        </div>
        <div class="item">
            <div class="holder">
                <div class="card"><img src="https://picsum.photos/100/100?2" alt="kep 2">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <span class="name">Ad modi</span></div>
                <p>Aperiam aspernatur aut commodi culpa cum cumque dignissimos dolore dolorum hic, id incidunt inventore
                    iusto libero maiores minus nobis perferendis porro, quae, quisquam temporibus ut?</p>
            </div>
        </div>
        <div class="item">
            <div class="holder">
                <div class="card"><img src="https://picsum.photos/100/100?3" alt="kep 3">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <span class="name">Magni saepe</span></div>
                <p>Dignissimos exercitationem nemo officiis quisquam quos! Amet consectetur consequatur deleniti earum
                    eligendi explicabo facere, laborum possimus quae quasi qui quisquam, tempora unde velit vitae
                    voluptates.</p>
            </div>
        </div>
        <div class="item">
            <div class="holder">
                <div class="card"><img src="https://picsum.photos/100/100?4" alt="kep 4">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <span class="name">Repellendus reprehenderit</span></div>
                <p>Assumenda beatae blanditiis culpa cumque dolore doloremque dolorum eum fugit itaque iusto labore,
                    natus
                    nesciunt odio officia, quibusdam quos unde? Id obcaecati totam voluptate voluptatibus.</p>
            </div>
        </div>
        <div class="item">
            <div class="holder">
                <div class="card"><img src="https://picsum.photos/100/100?5" alt="kep 5">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <span class="name">Necessitatibus nihil</span></div>
                <p>Ad alias aperiam architecto autem consectetur corporis deleniti esse, est fuga illum ipsa iusto
                    laboriosam numquam officia quos. Dolores dolorum in iusto nisi sint totam.</p>
            </div>
        </div>
        <div class="item">
            <div class="holder">
                <div class="card"><img src="https://picsum.photos/100/100?6" alt="kep 6">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <span class="name">Ex necessitatibus</span></div>
                <p>Ab labore laboriosam magnam molestias, perferendis quis tempora vero! Debitis eaque eius, eligendi
                    itaque
                    iure magnam maxime nemo reiciendis repellat similique soluta temporibus voluptatibus voluptatum.</p>
            </div>
        </div>
    </div>
</section>
<?php include "includes/recent_posts.php"; //a blogot includeoljuk?>
<section id="contact" class="wrapper">
    <h2 class="subheading orange-text">contact</h2>
    <h1 class="dark-text">get in touch</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam debitis facere quia, quo quod reiciendis
        veritatis. Ad illo odit officia?</p>
    <div class="grid-holder grid-1-2">
        <div>
            <ul class="info">
                <li><b>email</b> hgy@iworkshop.hu</li>
                <li><b>phone</b> +3614500110</li>
                <li><b>address</b> 1139. Budapest, Frangepán u. 3</li>
                <li><b>website</b> <a href="https://ruander.hu" target="_blank">Ruander Oktatóközpont</a></li>
                <li class="social">
                    <a href="#"><i class="fa fa-google"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </li>
            </ul>
        </div>
        <div class="contact-form">
            <form method="get" class="form-grid">
                <div class="grid-holder grid-1-1">
                    <label for="name"><input type="text" name="name" id="name" placeholder="Gipsz Jakab"></label>
                    <label for="email"><input type="email" name="email" id="email" placeholder="email@cim.hu"></label>
                </div>
                <label for="subject"><input type="text" name="subject" id="subject" placeholder="tárgy"></label>
                <label for="msg"><textarea name="msg" id="msg" cols="30" rows="10"
                                           placeholder="your message"></textarea></label>
                <div>
                    <button class="btn type-1 animate__animated animate__jello">send</button>
                </div>
            </form>
        </div>
    </div>
</section>
<footer class="copyright greyBg">
    2020 &copy; Ruander Oktatóközpont - <a href="http://themetrading.com/html/runaway/template/regular/index-5.html"
                                           target="_blank">kiinduló sablon</a>
</footer>
<script>
    let menuToggleBtn = document.querySelector('#menuToggle')
    menuToggleBtn.onclick = function () {
        menuToggle();
    }

    function menuToggle() {
        let elements = document.querySelectorAll('ul.mainmenu li:not(.logo)') //halmaz
        console.log(elements)
        //halmaz bejárása
        //menüpontok mostani állapota
        let current = elements[0].style.display
        console.log(current)
        let style = 'none'//alapbol legyen none mondjuk
        if (current !== 'list-item') {//mert első betöltéskor '' utána mi állitjuk 'none'-ra;logikai op vagy -> || ; és -> &&
            style = 'list-item' //ha már none akkor legyen inkább list-item
        }
        for (let i = 0; i < elements.length; i++) {
            console.log(elements[i])
            elements[i].style.display = style //beállítjuk a kívánt stílust a menüpontokra
        }
    }

    //menuToggle()

    /***potfolio grid beállítások a w3schools alapján***/
    filterSelection("all") // Execute the function and show all columns
    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("column");
        if (c == "all") c = "";
        // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    // Show filtered elements
    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
                element.className += " " + arr2[i];
            }
        }
    }

    // Hide elements that are not selected
    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

    // Add active class to the current button (highlight it)
    var btnContainer = document.getElementById("myBtnContainer");
    var btns = btnContainer.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
</script>
<!--jquery betöltése cdnről-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/main.js"></script>
<!--owl carousel-->
<script src="js/owl.carousel.js"></script>
<!--waypoints-->
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script>
    //sticky menu
    $(window).scroll(function () {
        let menu = $('#main'),
            pos = $(window).scrollTop()
        if (pos > 75) {
            menu.addClass('sticky') //native js elem.classList.add()
        } else {
            menu.removeClass('sticky')
        }
        //console.log($(window).scrollTop())
    })
    //számlálók
    $('.value').counterUp({
        delay: 10,
        time: 3000
    });
    $('.value').addClass('animated fadeInDownBig');
    $('h3').addClass('animated fadeIn');
    //owl
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            }/*,
            1000: {
                items: 5
            }*/
        }
    })
    let ablak = $(window.top).height()
    $('#slider').css('height',ablak + 'px')
    console.log('Az ablak magassága: ' + $(window.top).height());
</script>
</body>
</html>