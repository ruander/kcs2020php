<?php
//db kapcsolat
require "connect.php";

//lekérés összeállítása
$qry = "SELECT officecode,country,city,phone FROM offices";
//lekérés
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
/*//kibontás ciklusban
while( false !== $row = mysqli_fetch_assoc($result)){
    echo '<pre>'.var_export($row,true).'</pre>';
}*/
//táblázat készítése az adatokból - CRUD tábla
$table = '<a href="?action=create">Új felvitel</a>
<table border="1">
            <tr>
             <th>irodakód</th>
             <th>ország</th>
             <th>város</th>
             <th>telefon</th>
             <th>művelet</th>
            </tr>';//table nyitás a cimsorral
//adatsorok
while($row = mysqli_fetch_assoc($result)){
    $table .= "<tr>
                <td>{$row['officecode']}</td>
                <td>{$row['country']}</td>
                <td>{$row['city']}</td>
                <td>{$row['phone']}</td>
                <td> 
                    <a href=\"?action=update&amp;id={$row['officecode']}\">módosít</a> | 
                    <a href=\"?action=delete&amp;id={$row['officecode']}\">töröl</a>
                </td>
               </tr>";
}
$table .= '</table>';//table zárása
//kiírás egy lépésben
echo $table;
/*@todo: sql kérések 16,17,18 megoldása php fileban*/