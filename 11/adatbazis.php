<?php
//db kapcsolat
require "connect.php";
//adatok lekérése
//kérés összeállítása változóba
$qry = "SELECT * FROM employees";
//kérés futtatása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
//egy sor kibontása
$row = mysqli_fetch_row($result);
//pre
echo '<pre>';
//válasz sorainak száma: var_dump(mysqli_num_rows($result));//23
var_dump($row);
//egy sor kibontása asszociatív tömbbe
$row = mysqli_fetch_assoc($result);
var_dump($row);
//ez pazarlás:
$row = mysqli_fetch_array($result);
var_dump($row);
//kibontás objektumba
$row = mysqli_fetch_object($result);
var_dump($row);
//összes sor kibontása egyszerre
$rows = mysqli_fetch_all($result,MYSQLI_ASSOC);
//var_dump($rows);
$output = '<h1>Alkalmazottak</h1>';
//kapott eredmények bejárása
foreach ($rows as $employee){
    //var_dump($employee);
    //kapott rekord értékeinek kilistázása ciklussal
    $output .= '<ul>';
    foreach($employee as $mezonev => $ertek){
        $output .="<li><b>$mezonev:</b> $ertek</li>";
    }
    $output .='</ul>';
}
//kiírás egy lépésben
echo $output;