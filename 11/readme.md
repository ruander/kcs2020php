##Adatbázis műveletek 

 Új felvitel (új iroda felvitele):
 INSERT INTO 
    `offices` (`officeCode`, `city`, `phone`, `addressLine1`, `addressLine2`, `state`, `country`, `postalCode`, `territory`) 
    VALUES ('8', 'Budapest', '+3611234567', 'Frangepán u. 3.', NULL, NULL, 'Hungary', 'H-1139', 'EMEA');
 ___
 (employee)
 INSERT INTO 
    `employees` (`employeeNumber`, `lastName`, `firstName`, `extension`, `email`           , `officeCode`, `reportsTo`, `jobTitle`) 
         VALUES ('7000'          , 'Horváth' , 'György'   , 'x7000'    , 'hgy@iworkshop.hu', '8'         , '1002'     , 'Mindenes');
 ___
 
 Adat módosítása 
 UPDATE `employees` SET `jobTitle` = 'Mindenes árvíz' AND mezőnév = 'érték' WHERE `employees`.`employeeNumber` = 7000
 
 ___
 Adatsor törlése
 
 DELETE FROM tablanev WHERE mezonev = 'érték'