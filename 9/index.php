<?php

/*
@todo HF
  1. a szelvénylista alján legyen egy sorsolás gomb
    megnyomásakor készítsük el a sorsolt számsort, és nézzük végig a szelvényeket
    a nyerteseket tároljuk el egy ./nyertesek/lotto-5-90-2020-10-15-18-33-12.json fileba
                                                         éééé-hh-nn-óó-pp-mm
            -a találatok számával és a találatokkal
    -a sorsolás után az eredeti file-t töröljük, így ujraindul a szelvények feladása
  @todo HF
    2. a szelvény listában legyen lehetőség játékonként generálni 10(100) szelvényt véletlenszerűen,  érdemes ezt paraméterezhető eljárásba tenni
        pl abc(rand(1,1000)@test.com - 1,5,34,56,78
    */
//file betöltése, mintha ide lenne gépelve
/*include "settings.php";*/
include_once "settings.php";
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Lottójáték - válassz játéktípust</title>
</head>
<body>
<h1>Lottójáték</h1>
<h2>Válassz játéktípust</h2>
<?php
/*ciklussal oldjuk meg a játéktipusok menüvé alakítását*/
$menu =  '<nav><ul>';//menu elemek nyitása
foreach($valid_gametypes as $k => $v){
    $menu .= '<li><a href="lotto.php?gametype='.$k.'">'.$k.'/'.$v.' játék</a> - <a href="adatbeolvas.php?gametype='.$k.'">eddigi szelvények</a></li>';
}
//menuelemek zárása
$menu .= '</ul></nav>';
// kiírás 1 lépésben
echo $menu;
?>
</body>
</html>