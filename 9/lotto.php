<?php
//settings betöltése
/*require "settings.php";*/
require_once "settings.php";//$valid_gametypes tömb
$gametype = filter_input(INPUT_GET, 'gametype', FILTER_VALIDATE_INT);

//benne van-e a tömbben mint kulcs
if (!array_key_exists($gametype, $valid_gametypes)) {
    //átirányítunk az indexre választani
    header('location:index.php');
    //biztonsági exit
    exit();
}
//érvényes gametype
$huzasok_szama = $gametype;
$limit = $valid_gametypes[$gametype];

//ha kapunk űrlap elemeket, kezeljük őket
if (!empty($_POST)) {
    $hiba = [];
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //tippek
    $tippek = filter_input(INPUT_POST, 'tippek', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    if (is_array($tippek)) {
        //szűrő opciók:
        $options = [
            'options' => [
                'min_range' => 1,
                'max_range' => $limit
            ]
        ];
        //ismétlődések kezelése
        $tippek_unique = array_unique($tippek);

        foreach ($tippek as $k => $v) {
            $test = filter_var($v, FILTER_VALIDATE_INT, $options);
            if (!$test) {
                $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
            } elseif (!array_key_exists($k, $tippek_unique)) {
                //benne van e a unique tippek tömbben az aktuális kulcs, mert ha nincs, akkor ismétlődés volt és kikerült belőle
                $hiba['tippek'][$k] = '<span class="error">Ezt már tippelted!</span>';
            }
        }

        //echo '<pre>' . var_export($tippek_unique, true) . '</pre>';
    }

    if (empty($hiba)) {
        //die('minden oké');
        //adatok rendezése

        //filenév
        $fileName = 'lotto-'.$huzasok_szama.'-'.$limit.'.json';
        //ha létezik már a file, töltsük be a tartalmát
        if(file_exists($dir.$fileName)){
            $fileJson = file_get_contents($dir.$fileName);
            $szelvenyek = json_decode($fileJson, true);
        }else{
            $szelvenyek = [];//itt gyűlnek a szelvények
        }

        //szelvény
        sort($tippek);
        $szelveny = [
            'email' => $email,
            'tippek' => $tippek

        ];
        array_push($szelvenyek, $szelveny);
        //var_dump('<pre>', $szelvenyek);
        $szelvenyekJson = json_encode($szelvenyek);
        //file kiírása
        $success = file_put_contents($dir.$fileName,$szelvenyekJson);
        if($success){//ha sikerült ujrainditjuk a folyamatot az inedxre irányítással
            header('location:index.php');
            exit();
        }
    }
}

//űrlap összeállítása változóba
$form = '<form method="post">';//űrlap nyitás
for ($i = 1; $i <= $huzasok_szama; $i++) {
    //input elemek elkészítése a tippeknek
    $form .= '<label style="display:block">
               Tipp <input maxlength="2" size="2" type="text" value="' . getValue('tippek', $i) . '" name="tippek[' . $i . ']" placeholder="1-' . $limit . '">';
    //hiba befűzése a mezőbe
    $form .= isset($hiba['tippek'][$i]) ? $hiba['tippek'][$i] : '';
    $form .= '</label>';//mezőcimke lezárása
}
//email mező beillesztése
$form .= '<label style="display:block">
               Email <input type="text" value="' . getValue('email') . '" name="email" placeholder="you@example.com">';
//hiba befűzése a mezőbe
$form .= isset($hiba['email']) ? $hiba['email'] : '';
$form .= '</label>';
$form .= '<button>Tippek beküldése</button>
         </form>';//űrlap zárás
//kiírás egy lépésben
echo $form;

/*saját eljárások gyüjteménye*/
/**
 * @param $fieldName :string | a mező neve
 * @param false $subKey :mixed | ha tömbben vannak az értékek a postban akkor 2 szint kulcsa
 * @return string - beírandó érték a mezőbe
 */
function getValue($fieldName, $subKey = false)
{
    //ha kapunk subkey-t, akkor szűrő kell
    if ($subKey !== false) {
        $elem = filter_input(INPUT_POST, $fieldName, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);//leszűrjük a tömbböt default szűrővel
        if (isset($elem[$subKey])) {//ha létezik az adott elem
            return $elem[$subKey];//visszatérünk a kívánt elemmel
        }
    } else {//nem kaptunk subkeyt tehát első szinten megtaláljuk az adatot postban, visszatérünk vele
        return filter_input(INPUT_POST, $fieldName);
    }
    return false;
}
