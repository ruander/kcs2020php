<?php
    //@todo:legyen egy opció hogy 'inkább játszani szeretnék'
    //@todo: a játéknál legyen egy gomb hogy 'szelvénylista'
//settings
require_once "settings.php";
$gametype = filter_input(INPUT_GET, 'gametype',FILTER_VALIDATE_INT);
if(array_key_exists($gametype,$valid_gametypes)){//kaptunk érvényes játéktipust, ha létezik a file, olvassuk be
 $fileName = 'lotto-'.$gametype.'-'.$valid_gametypes[$gametype].'.json';

 if(file_exists($dir.$fileName)){
     $szelvenyekJson = file_get_contents($dir.$fileName);
     $szelvenyek = json_decode($szelvenyekJson,true);
 }else{
     $szelvenyek = [];//ha nincsenek még tippek egyáltalán azaz file sincs akkor legyen ures tomb a szelvenyek tombje
 }

}else {//nem kaptunk játéktipust , felépitjuk a választo menut
    $menu = '<nav><ul>';//menu elemek nyitása
    foreach ($valid_gametypes as $k => $v) {
        $menu .= '<li><a href="?gametype=' . $k . '">' . $k . '/' . $v . ' játék</a></li>';
    }
    $menu .= '<li><a href="index.php">inkább játszani szeretnék</a>';
//menuelemek zárása
    $menu .= '</ul></nav>';
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Szelvények beolvasása</title>
</head>
<body>

<?php

if(isset($menu)){
    //választó menü van
    echo '<h1>Szelvények beolvasása</h1>
          <h2>Válassz játéktípust</h2>';
    echo $menu;//menu kiírása
}else{
//szelvények vannak
    echo "<h1>A $gametype/$valid_gametypes[$gametype] játék szelvényei</h1>";
    $table = '<table border="1">
                <tr>
                  <th>sorszám</th>
                  <th>email</th>
                  <th>tippsor</th>
                </tr>';//címsor és tábla nyitása
    //szelvények
    foreach($szelvenyek as $szelveny_id => $szelveny){
        //echo '<pre>'.var_export($szelveny,true).'</pre>';
        $table .= '<tr>
                    <td>'.$szelveny_id.'</td>
                    <td>'.$szelveny['email'].'</td>
                    <td>'.implode(',',$szelveny['tippek']).'</td>
                  </tr>';
    }
    $table .= '</table>
                <a href="adatbeolvas.php">Vissza a menübe</a>';

    echo $table;
}



?>
</body>
</html>